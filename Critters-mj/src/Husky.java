//Maggie Jiang
//2.9.19
//Period 1 APCS
//This class defines the behavior of Husky objects in the simulation

import java.awt.Color;


public class Husky extends Critter{
	
	//creates purple and gold colors
	public static final Color PURPLE = new Color(102, 0, 153);
	public static final Color GOLD = new Color(255, 204, 51);
	
	//helps determine color during that turn
	private boolean isPurple;
	
	//constructs a husky
	public Husky(){
		isPurple=true;
	}
	
	//eats if no other critters are right next to it
	public boolean eat() {
		//if all four directions are clear or are food, eat
		if (((getNeighbor(Direction.NORTH).equals(" ")) || (getNeighbor(Direction.NORTH).equals("."))) 
		&& ((getNeighbor(Direction.EAST).equals(" ")) || (getNeighbor(Direction.EAST).equals(".")))
		&& ((getNeighbor(Direction.SOUTH).equals(" ")) || (getNeighbor(Direction.SOUTH).equals(".")))
		&& ((getNeighbor(Direction.WEST).equals(" ")) || (getNeighbor(Direction.WEST).equals("."))))
		return true;
		
		//if critter in any neighboring square, don't eat
		return false;
	}
	
	//always beats ants, birds/vultures, and non hungry hippos, but randomly attacks against hungry hippos
	public Attack fight(String opponent) {
		
		if(opponent.equals("%")) return Attack.ROAR;
		if(opponent.equals("0") || opponent.equals("^") || opponent.equals(">") || opponent.equals("V") || opponent.equals("<")) return Attack.SCRATCH;
		
		//random attack
		int randomInt=(int)(Math.random()*3);
		if (randomInt==0){
			return Attack.ROAR;
		}
		
		if (randomInt==1){
			return Attack.POUNCE;
		}
		
		else{
			return Attack.SCRATCH;
		}
	}
	
	//alternates between purple and gold
	public Color getColor() {
		this.isPurple=!isPurple;
		if (isPurple) return PURPLE;
		return GOLD;
	}

	//random movement
	public Direction getMove() {
		int randomInt=(int)(Math.random()*4);
		
		if (randomInt==0){
			return Direction.NORTH;
		}
		
		if (randomInt==1){
			return Direction.EAST;
		}
		
		if (randomInt==2){
			return Direction.SOUTH;
		} 
		
		else{
			return Direction.WEST;
		}
	}
	
	//represented by W
	public String toString() {
		return "W";
	}
}
