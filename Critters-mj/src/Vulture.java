//Maggie Jiang
//2.9.19
//Period 1 APCS
//This class defines the behavior of Vulture objects in the simulation

import java.awt.Color;

public class Vulture extends Bird{
	
	private boolean isHungry; //for eating habits
	
	//constructor
	public Vulture(){
		super();
		isHungry=true; //it is initially hungry
	}
	
	//vultures are black
	public Color getColor() {
		return Color.BLACK;
	}
	
	//eats only if it is hungry
	public boolean eat() {
		if(isHungry){
			isHungry=false; //is not hungry anymore after it has eaten
			return true;
		}
		return false;
	}
	
	//becomes hungry again after fighting
	public Attack fight(String opponent) {
		isHungry=true;
		return super.fight(opponent);
	}
}
