//Maggie Jiang
//2.9.19
//Period 1 APCS
//This class defines the behavior of Bird objects in the simulation


import java.awt.Color;

public class Bird extends Critter{
	
	//current direction to move the clockwise square
	private Direction movement;
	
	//number of moves bird has moved in one direction in one given set of 3
	private int numMoves;
	
	//constructs a bird
	public Bird(){
		movement=Direction.NORTH;
		numMoves=0;
	}
	
	//roars if opponent is an ant, otherwise pounces
	public Attack fight(String opponent) {
		if(opponent.equals("%")) return Attack.ROAR;
		return Attack.POUNCE;
	}
	
	//birds are blue
	public Color getColor() {
		return Color.BLUE;
	}

	//moves in a clockwise 3x3 square
	public Direction getMove() {
		
		//moves three times in one direction
		if (numMoves<3){
			numMoves++;
			
		} else{ //updates direction after three moves
			if (movement.equals(Direction.NORTH)){
				this.movement=Direction.EAST;
			} else if (movement.equals(Direction.EAST)){
				this.movement=Direction.SOUTH;
			} else if (movement.equals(Direction.SOUTH)){
				this.movement=Direction.WEST;
			} else{
				this.movement=Direction.NORTH;
			}
			this.numMoves=1;
		}
		
		return movement;
	}
		
	//depends on which direction it has moved
	public String toString() {
		if (movement.equals(Direction.EAST)){
			return ">";
		} else if (movement.equals(Direction.SOUTH)){
			return "V";
		} else if (movement.equals(Direction.WEST)){
			return "<";
		} else return "^";
	}
}
