//Maggie Jiang
//2.9.19
//Period 1 APCS
//This class defines the behavior of Hippo objects in the simulation

import java.awt.Color;



public class Hippo extends Critter{
	
	//how many more times it wants to eat
	private int hunger;
	
	//number of times it has moved in its current direction during the current cycle
	private int numMoves;
	
	//direction it will move in
	private Direction moveD;
	
	//constructor
	public Hippo(int hunger){
		this.hunger=hunger;
		this.numMoves=5;
	}
	
	//eats for hunger amount of times
	public boolean eat() {
		if (hunger>0) {
		hunger--; //updates after it eats
		return true;
		}
		return false;
	}
	
	//scratches when hungry, otherwise pounces
	public Attack fight(String opponent) {
		if (hunger>0) return Attack.SCRATCH;
		return Attack.POUNCE;
	}
	
	//grey when hungry, otherwise white
	public Color getColor() {
		if (hunger>0) return Color.GRAY;
		return Color.WHITE;
	}

	//randomly picks a direction, moves five times, repeat
	public Direction getMove() {
		
		//moves five times in one direction
		if (numMoves<5){
			numMoves++;
			return moveD;
		} 
		
		else{ //updates direction after five moves
			this.numMoves=1;
			int randomInt=(int)(Math.random()*4);
			
			if (randomInt==0){
				this.moveD= Direction.NORTH;
				return Direction.NORTH;
			}
			
			if (randomInt==1){
				this.moveD= Direction.EAST;
				return Direction.EAST;
			}
			
			if (randomInt==2){
				this.moveD= Direction.SOUTH;
				return Direction.SOUTH;
			} 
			
			else{
				this.moveD= Direction.WEST;
				return Direction.WEST;
			}
		}	
		
	}

	//displayed as the number of times it can still eat
	public String toString() {
		return "" + hunger;
	}
	
}
