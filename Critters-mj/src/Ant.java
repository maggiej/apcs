/*
 * Grading:  22/20
 * Deductions:
 * 	Project Name: Ok
 * 	Comments: Ok
 * 	Authorship:Ok
 * 	Style: Ok
 * 
 * Implementation:
 * 	Ant: Ok
 * 	Bird: Ok
 * 	Hippo: Ok
 * 	Vulture: Good!
 * 	Husky (EC): +2 (I liked the purple and gold husky!)
 * 	 
 
 * Look for TODO comments in your code (or search on "tasks" in the quick access window) to quickly view all of them
 * Please understand what you did wrong and avoid these problems in the future!
 * 
 * General comments:   Note:  it's good style to prefix your field names with a _ it clearer that they are fields in your code (e.g. _walkSouth) 
 * 	Otherwise, well done!
 * 
 */


//Maggie Jiang
//2.9.19
//Period 1 APCS
//This class defines the behavior of Ant objects in the simulation

import java.awt.Color;

public class Ant extends Critter {
	
	//both fields are for movement
	private boolean walkSouth; 
	private boolean east;
	
	//constructs an Ant
	public Ant(boolean walkSouth){
		this.walkSouth=walkSouth;
		this.east=true;
	}
	
	//ants always eat
	public boolean eat() {
		return true;
	}
	
	//ants always scratch
	public Attack fight(String opponent) {
		return Attack.SCRATCH;
	}

	//ants are red
	public Color getColor() {
		return Color.RED;
	}
	
	//SESE if walkSouth is true, NENE if false
	public Direction getMove() {
		
		//east field is updated every move so ant alternates between east and N/S.
		//moves east when east is true.
		this.east=!east; //first move is not east
		
		if(east){
			return Direction.EAST;
		}
		
		if(walkSouth){
			return Direction.SOUTH;	
		}
		
		else{
			return Direction.NORTH;
		}
		
	}

	//ants are represented by %
	public String toString() {
		return "%";
	}
}
