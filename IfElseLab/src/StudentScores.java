/*
 * Grading:  12/12
 * Deductions 
 * 
 * Look for TODO comments in your code (or search on "tasks" in the quick access window) to quickly view all of them
 * Please understand what you did wrong and avoid these problems in the future!
 * 
 * General comments:
 * Well done Maggie!
 */

// Maggie Jiang
// 11.04.18
// Period 1 APCS
/* This program prompts the user for two students' percentage scores 
   (0-100), computes a grade between 0.0 and 4.0, and outputs whether 
   the student passed the course as well as whether the student may 
   advance.
 */

import java.util.*;
public class StudentScores {
	
	static Scanner console = new Scanner(System.in);
	
	// prompts user for percentage scores, computes grade, outputs information
	public static void main(String[] args) {
		System.out.println("This program converts two scores into grades.");
		System.out.println();
		printResults(1); // for first student
		printResults(2); // for second student
	}
	
	// this method takes a score, calculates a grade, prints the grade, and returns the grade
	public static double calculateGrade (int studentNumber){
		
		// prompts user for percentage score and stores it
		System.out.println("Student " + studentNumber + ":");
		System.out.print("What score did you get? ");
		double score = Math.round(console.nextDouble()); // rounds percentage to nearest whole number
		
		// calculates grade from percentage score
		double grade;
		if (score>=95){
			grade = 4.0;
		} else if(score < 95 && score >=62) {
			grade = 4.0-(.1*(95-score));
			grade = (Math.round(grade*10))/10.0; // ensures grade goes to tenths place
		} else if(score ==61 || score ==60) {
			grade = 0.7;
		} else {
			grade = 0.0;
		}
		
		// prints out grade
		System.out.println("You received a grade of " + grade);
		return grade;
	}
	
	// this method takes the calculated grade and prints out pass/fail and advance/retake info
	public static void printResults(int studentNumber){
		
		double grade=calculateGrade(studentNumber);
		
		// prints out whether student passed or failed based on grade
		if (grade > 0.0){
			System.out.println("You passed the course");
		} else {
			System.out.println("You failed the course");
		}
		
		// prints out whether student has to retake the course based on grade
		if (grade >=2.0){
			System.out.println("You may advance to the next course");
		} else {
			System.out.println("You must take the course again");
		}	
		System.out.println();
	}
	
}  