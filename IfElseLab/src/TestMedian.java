// Maggie Jiang
// 11.04.18
// Period 1 APCS
/* This program tests the median method, which accepts three different
   integers as parameters and returns the middle value of the three.
 */

public class TestMedian {
	
	// tests several cases of the median method
	public static void main(String[] args) {
		System.out.println(median(7, 3, 9)); // should print 7
		System.out.println(median(29, -14, 11)); // should print 11
		System.out.println(median(752, 64, 121)); // should print 121
	}
	
	// determines which integer out of three is the middle value
    public static int median(int one, int two, int three) {
    	int answer;
    	if ((one < two && one > three) || (one > two && one < three)){
    		answer=one;
    	} else if ((two < one && two > three) || (two > one && two < three)){
    		answer=two;
    	} else {
    		answer=three;
    	}
    	return answer;
    }
} 