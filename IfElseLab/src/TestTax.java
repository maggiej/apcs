// Maggie Jiang
// 11.04.18
// Period 1 APCS
/* This program tests the tax method, which takes a positive salary as a 
   parameter and returns the amount of federal tax someone would owe if
   they made that salary.
 */

public class TestTax {
	
	// tests several cases of the tax method
    public static void main(String[] args) {
        System.out.println("The tax on $50000 is " + tax(50000)); // tax should be 9237.5
        System.out.println("The tax on $27500 is " + tax(27500)); // tax should be 3767.5
        System.out.println("The tax on $6000 is " + tax(6000)); // tax should be 600.0
        System.out.println("The tax on $120000 is " + tax(120000)); // tax should be 28227.0
    }
    
    // calculates tax owed based on salary and tax bracket
    // tax bracket dependent on salary
    public static double tax(double salary) {
    	double fedTax;
    	if (salary <= 7150) { 				// tax bracket 1
    		fedTax=.1*salary;
    	} else if (salary <=29050) { 		// tax bracket 2
    		fedTax=715+.15*(salary-7150);
    	} else if (salary <=70350) { 		// tax bracket 3
    		fedTax=4000+.25*(salary-29050);
    	} else { 							// tax bracket 4
    		fedTax=14325+.28*(salary-70350);
    	}
    	return fedTax;
    }
}
    
