// Maggie Jiang
// 11.08.18
// Period 1 APCS
/* This program prompts the user for a number of guests' dinner costs, 
 * sums the costs up, calculates the subtotal, tax, tip, and total price, 
 * and prints out the results.
 */

import java.util.*;
public class Receipt {
	static Scanner console = new Scanner(System.in);
    public static void main(String[] args) {
        calculateReceipt();
    }
    
    // calculates and prints out receipt info: subtotal, tax, tip, total.
    public static void calculateReceipt() {
    	// prompts user for number of guests and saves result.
    	System.out.print("How many people ate? ");
    	int people = console.nextInt();
    	
    	// sums up the costs to calculate subtotal and prints result.
    	double subtotal = 0;
    	for (int i =1; i<= people; i++) {
    		System.out.print("Cost for person " + i + "? " );
    		subtotal = subtotal + console.nextDouble();
    	}
    	System.out.println("Subtotal: " + subtotal);
    	
    	// defines conditions for tax and tip.
    	double tax = subtotal * .08;
    	double tip = subtotal * .15;
    	
    	// calculates tax and tip and prints those results as well as total cost.
    	System.out.println("Tax: " + tax);
        System.out.println("Tip: " + tip);
        System.out.println("Total: " + (subtotal + tip + tax));
    	
    } 
}