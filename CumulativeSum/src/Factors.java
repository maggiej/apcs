// Maggie Jiang
// 11.08.18
// Period 1 APCS
/* This program contains a method that determines the number of factors for a non-negative integer 
 * and tests that method.
 */

public class Factors {
	
	// Tests three cases of the countFactors method.
	public static void main(String[] args) {
		int test1 = countFactors(4);  // 3
		int test2 = countFactors(7);  // 2
		int test3 = countFactors(24); // 8
		
		boolean passed1 = checkTest("Test 1", test1, 3);
		boolean passed2 = checkTest("Test 2", test2, 2);
		boolean passed3 = checkTest("Test 3", test3, 8);
		
		if (passed1 && passed2 && passed3) {
			System.out.println("All tests passed!");
		}
	}
	
	// Returns the number of factors for a non-negative integer.
	public static int countFactors(int num) {
		int numFactors = 0;
		for (int i = 1; i <= num; i++) {
			if (num%i == 0) {
				numFactors++;
			}
		}
		
		if (numFactors == 0) {
			return -1;
		} else {
			return numFactors;
		}
	}
	
	// Tests the countFactors method.
	private static boolean checkTest(String name, int actual, int expected) {
		boolean passed = actual == expected;
		if (!passed) {
			System.out.println(name + " failed.");
			System.out.println("Expected " + expected +
					           ", but response was " + actual);
			System.out.println();
		}
		
		return passed;
	}
}
