/*
 * Grading:  70/70
 * 
 * Deductions:
 *  	Checkpoints/Branches: Ok
 * 		Main Method: Ok
 * 		Unit Tests: Ok
 * 		Authorship:Ok
 * 		Comments: Ok
 * 		Style: Ok
 * 		Extra Credit: n/a (I should really deduct points for you on this...)
 * 
 * Look for TODO comments in your code (or search on "tasks" in the quick access window) to quickly view all of them
 * Please understand what you did wrong and avoid these problems in the future!
 * 
 * General comments:  	Fine job Maggie - I've added some TODOs below, but just keep focus on writing smarter, more elegant code.   (and show up to class on time... ;-))
 * 
 */

//Maggie Jiang
//Period 1 APCS
//Checkpoint 4: evaluates complex expressions with +-*/ from left to right

package calculator;
import java.util.*;

public class Calculator 
{
    public static void main(String[] args) 
    {
        // TODO: Read the input from the user and call produceAnswer with an equation
    	
    	//TODO: Close scanner when done.
    	Scanner console = new Scanner(System.in);
    	
    	//prompts for user input until they decide to quit
    	while(true) {
    		System.out.println("Enter a simple expression or \"quit\" to stop.");
    		String input = console.nextLine();
    		if (input.equalsIgnoreCase("quit")) break; //stops when they decide to quit
    		System.out.println(produceAnswer(input));
    	}
    }
       
    /**
     *    ** IMPORTANT ** DO NOT DELETE THIS FUNCTION.  This function will be used to test your code.
     *  
     *  This function takes a String 'input' and produces the result. The input is a string that needs to be evaluated.  
     *  For your program, this will be the user input. For example, 
     *          input ==> "1 + 3"
     *  the function should return the result of the expression after it has been calculated:
     *          return ==> "4"
     *  If there is an error in the user input, the appropriate error message is returned.
     *           
     * @param input     the arithmetic expression to be evaluated
     * @return          the result of the arithmetic expression or an error message
     */
    
    // TODO: Implement this function to produce the solution to the input
    //checkpoint 4: evaluates complex +-*/ expressions left to right
    public static String produceAnswer(String input){
    	
    	//uses the method built in cp3 (evaluates one simple expression) to evaluate the input one simple expression at a time
    	while(true){
    		if(input.indexOf(" ")==-1){ //if there's only one token
    			return evalSimpleExpression(input);
    		}
    	
    		String op1 = getFirstWord(input); //stores first token
    		input = skipFirstWord(input); //everything past first token
    		
    		if(input.indexOf(" ")==-1) {
    			return evalSimpleExpression(op1 + " " + input); //if there's only two tokens 
    		}
    		
    		String operator = getFirstWord(input); //stores operator
    		input = skipFirstWord(input);//everything past second token
    		
    		if(input.indexOf(" ")==-1){ //if there's only three tokens
    			return evalSimpleExpression(op1 + " " + operator + " " + input);
    		}
    		
    		String op2 = getFirstWord(input);
    		input = skipFirstWord(input); //past third token
    		String oneSimpleExpression = op1 + " " + operator + " " + op2; //stores first set of three tokens
    		
    		//TODO: You should avoid calling this function *three* times - Instead store in a variable and return that if necessary.
    		if(evalSimpleExpression(oneSimpleExpression).charAt(0)=='<'){ //terminates if set of 3 evaluates to an error
    			return evalSimpleExpression(oneSimpleExpression);
    		}
    		
    		input = evalSimpleExpression(oneSimpleExpression) + " " + input; //input is updated after first operation is evaluated
    	}
    }
    
    //same method as CP3. evaluates one simple expression with three tokens.
    public static String evalSimpleExpression(String input)
    { 
    	
    	input=input.trim();
    	
    	if (input.indexOf(" ")==-1) { //if there's only one token return the token
    		if(isValidNumber(input) || input.equals("")) return input;
    		else return "<ERROR> Invalid value: " + input;
    	}
    	
    	String op1 = getFirstWord(input);
    	
    	if (!isValidNumber(op1)){ //if first operator is not a valid number
    		return "<ERROR> Invalid value: " + op1;
    	}
    	
    	input = skipFirstWord(input);
    	
    	if(input.indexOf(" ")==-1){ //if there's no token after the operator
    		if( !input.equals("+") && !input.equals("-") && !input.equals("*") && !input.equals("/")){ //invalid operator error takes precedence
        		return "<ERROR> Invalid operator encountered: " + input;
        	}else{
        		return "<ERROR> Invalid expression format.";
        	}
    	}
    	
    	String operator = getFirstWord(input);
    	
    	if( !operator.equals("+") && !operator.equals("-") && !operator.equals("*") && !operator.equals("/")){ //if operator is invalid
    		return "<ERROR> Invalid operator encountered: " + operator;
    	}
    	
    	input = skipFirstWord(input);
    	
    	if(input.indexOf(" ")!=-1){ //if there are more tokens than 3, making it a complex expression
    		return "<ERROR> Invalid expression format.";
    	}
    	
    	String op2 = input;
    	
    	if (op2.equals("0")&& operator.equals("/")) { //checks for divide by zero error
    		return "<ERROR> Cannot divide by zero.";
    	} else if (!isValidNumber(op2)){ //if second operator is not a valid number
    		return "<ERROR> Invalid value: " + op2;
    	}
    	
    	//evaluates the expression if valid.
    	if(operator.equals("+")) {
    		return Double.parseDouble(op1) + Double.parseDouble(op2) + "";
    	} else if (operator.equals("-")) {
    		return Double.parseDouble(op1) - Double.parseDouble(op2) + ""; 
    	} else if (operator.equals("*")) {
    		return Double.parseDouble(op1) * Double.parseDouble(op2) + "";
    	} else {
    		return Double.parseDouble(op1) / Double.parseDouble(op2) + "";
    	}
    	
    }
    
    //TODO: This isn't used anywhere - Why is it here?
    //returns the index value of the last space in a given string
    public static int indexOfLastSpace(String input, int inputLength){
    	int indexOfLastSpace = 0; 
        for (int i = 0; i<inputLength; i++){ //loops through length of given string
        	String charToCheck=input.charAt(i) + ""; //turns each char into a string
        	if(charToCheck.equals(" ")){ //if the char is a space, index of last space updates
        		indexOfLastSpace = i;
        	}
        }
        return indexOfLastSpace; //returns 0 if empty string or string w no spaces (1 token)
    }
    
    
    //returns the first token in a string input
    public static String getFirstWord(String input) {
    	input=input.trim();
    	return input.substring(0, input.indexOf(" ")); //from start to the first space
    }
    
    //returns string input - the first token
    public static String skipFirstWord(String input) { //from just past first space to end
    	input=input.trim();
    	return input.substring(input.indexOf(" ")+1);
    	
    }

    
    /**
     * You may find this function useful in determining if a string value is a valid integer or not. If you call Integer.parseInt()
     * on a string that is not a valid integer, a NumberFormatException will be thrown and your program will terminate. In order to 
     * not have your program terminate on invalid integers, call this function to determine if the string is a valid integer before 
     * parsing it as an integer value.  
     * 
     * @param strVal    the string expression to test to see if it's an integer
     * @return          true if string expression is an integer, false otherwise
     */
    public static boolean isValidNumber(String strVal)
    {
        try
        {
            Double.parseDouble(strVal);             
        }
        catch (NumberFormatException e) 
        {
            return false;
        }
        return true;        
    }
    
    /**
     * You may find the gcd() and lcm() functions helpful if you decide to do the fraction values extra credit work. 
     * These functions implement Euclid's algorithm for finding GCD (greatest common divisor) and LCM (least common multiple).
     *  
     * @param a     first number
     * @param b     second number
     * @return      greatest common divisor or least common multiple of a and b
     */   
    public static int gcd(int a, int b)
    {
        a = Math.abs(a); 
        b = Math.abs(b); 
        while (b > 0)
        {
            int temp = b;
            b = a % b;
            a = temp;
        }
        return a;
    }
    
    public static int lcm(int a, int b)
    {
        a = Math.abs(a); 
        b = Math.abs(b); 
        return a * (b / gcd(a, b));
    }   
    
    
    // TODO: Fill in the space below with any helper methods that you think you will need
    
}
