/*
 * Grading:  21/20
 * Deductions:  
 * 
 * Look for TODO comments in your code (or search on "tasks" in the quick access window) to quickly view all of them
 * Please understand what you did wrong and avoid these problems in the future!
 * 
 * General comments:
 * Well done Maggie - Just a minor thing - Add some blank lines in between your for loops, etc..   Makes it easier to read!
 */

//Maggie Jiang
//Period 1 AP CS
//10.9.18
//NestedLoops project

public class NestedLoops {
	
	public static final int TOTAL_ROWS = 4; //this constant is the number of rows in each triangle
	
	//main method draws an hourglass figure
	public static void main(String[] args) {
		drawBase();
		drawTopTriangle();
		drawBottomTriangle();
		drawBase();
	}  
	
	//draws the line that forms the very top and bottom of the hourglass
	public static void drawBase() {
		System.out.print("|");
		for(int i = 1; i <= 2*TOTAL_ROWS; i++) { //prints " 8 times
			System.out.print("\"");
		}
		System.out.println("'|");
	}
	
	//draws top triangle of hourglass
	public static void drawTopTriangle() {
		for(int i = 1; i <= TOTAL_ROWS; i++) { //four lines of some spaces, a backslash, some colons, a slash
			for(int j = 1; j <= i; j++) {
				System.out.print(" ");
			}
			System.out.print("\\");
			for(int j=1; j <= i*-2 + 2*TOTAL_ROWS+1; j++) {
				System.out.print(":");
			}
			System.out.println("/");
		}
	}
	
	//draws bottom triangle of hourglass
	public static void drawBottomTriangle() {
		for(int i = TOTAL_ROWS; i >= 1; i--) { //uses the same loop as top triangle but decrementing
			for(int j = 1; j <= i; j++) {
				System.out.print(" ");
			}
			System.out.print("/");
			for(int j = 1; j <= i*-2 + 2*TOTAL_ROWS+1; j++) {
				System.out.print(":");
			}
			System.out.println("\\");
		}
	}
}