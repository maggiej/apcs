/*
 * Grading: 10.5/10
 * Deductions:	 
 * 
 * Look for TODO comments in your code (or search on "tasks" in the quick access window) to quickly view all of them
 * Please understand what you did wrong and avoid these problems in the future!
 * 
 * General comments:  Well done Maggie (again...)
 * 
 */


//Maggie Jiang
//01.12.19
//Period 1 APCS
//This class is a blueprint for Date objects

//may assume all parameters passed are valid
public class Date {
	//fields of a date object: month and day
	int month;
	int day;
	
	//constructs new Date object
	public Date(int initialMonth, int initialDay){
		month=initialMonth;
		day=initialDay;
	}
	
	//returns month of Date object on which it's called
	public int getMonth(){
		return month;
	}
	
	//returns day of Date object on which it's called
	public int getDay(){
		return day;
	}
	
	//modifies state of Date object to represent given month and day
	public void setDate(int newMonth, int newDay){
		month=newMonth;
		day=newDay;
	}
	
	//returns String representation of date object
	public String toString(){
		return month + "/" + day;
	}
	
	//checks if two Date objects represent the same date
	public boolean equals(Date d){
		return month==d.month && day==d.day;
	}
	
	//returns number of days in the month represented by the Date object on which it was called
	public int daysInMonth(){
		if (month==2) return 28;
		if (month == 4 || month == 6 || month == 9 || month == 11) return 30;
		return 31;
	}
	
	//advances date one day in time
	public void nextDay(){
		if (day==daysInMonth()){
			if(month==12){
				month=1;
			}else{
				month++;
			}
			day=1;
		}else{
			day++;
		}
	}
}
