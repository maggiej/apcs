import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Date class unit tests.
 * 
 * @author jmeier
 */
public class Tests {

	private static final int[] DAYS_IN_MONTH = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

	@Test
	public void testCtor() {
		for (int i = 1; i <= 12; ++i) {
			for (int j = 1; j <= 10; ++j) {
				Date date = new Date(i, j);
			}
		}
	}

	@Test
	public void testCtorAndGetters() {
		for (int i = 1; i <= 12; ++i) {
			for (int j = 1; j <= 10; ++j) {
				Date date = new Date(i, j);
				assertEquals(i, date.getMonth());
				assertEquals(j, date.getDay());
			}
		}
	}

	@Test
	public void testSetDate() {
		for (int i = 1; i <= 11; ++i) {
			for (int j = 1; j <= 10; ++j) {
				Date date = new Date(i, j);
				date.setDate(i + 1, j + 1);
				assertEquals(i + 1, date.getMonth());
				assertEquals(j + 1, date.getDay());
			}
		}
	}
	
	@Test
	public void testToString() {
		for (int i = 1; i <= 12; ++i) {
			for (int j = 1; j <= 10; ++j) {
				Date date = new Date(i, j);
				assertEquals(i + "/" + j, date.toString());
			}
		}
	}
	
	@Test
	public void testEquals() {
		for (int i = 1; i <= 12; ++i) {
			Date theDate = new Date(i, 1);
			Date theDate2 = new Date(i, 1);
			Date nextMonth = new Date(i + 1, 1);
			Date nextDay = new Date(i, 2);
			assertTrue(theDate.equals(theDate2));
			assertTrue(!theDate.equals(nextMonth));
			assertTrue(!theDate.equals(nextDay));
		}
	}
	
	@Test
	public void testDaysInMonth() {
		for (int i = 1; i <= 12; ++i) {
			Date date = new Date(i, 1);
			assertEquals(DAYS_IN_MONTH[i - 1], date.daysInMonth());
		}
	}
	
	@Test
	public void testNextDay() {
		for (int i = 1; i <= 12; ++i) {
			for (int j = 1; j < DAYS_IN_MONTH[i - 1]; ++j) {
				Date date = new Date(i, j);
				date.nextDay();
				assertEquals(i, date.getMonth());
				assertEquals(j + 1, date.getDay());
			}
		}
	}

	@Test
	public void testNextDay_endOfMonth() {
		for (int i = 1; i <= 12; ++i) {
			Date date = new Date(i, DAYS_IN_MONTH[i - 1]);
			date.nextDay();
			assertEquals(i < 12 ? i + 1 : 1, date.getMonth());
			assertEquals(1, date.getDay());			
		}
	}
}
