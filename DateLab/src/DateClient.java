//Maggie Jiang
//01.12.19
//Period 1 APCS
//This program tests the instance methods of the Date class on three Date objects

public class DateClient {
	public static void main(String[] args){
		Date testEquals = new Date(1, 8); //to test equality later
		
		//tests for first Date object
		//same order for second and third Date objects.
		Date testOne = new Date(3, 3); //tests constructor
		System.out.println(testOne.getMonth()); //tests getMonth
		System.out.println(testOne.getDay()); //tests getDay
		testOne.setDate(2, 22); //tests setDate
		System.out.println(testOne); //tests toString
		System.out.println(testOne.equals(testEquals)); //tests equals
		System.out.println(testOne.daysInMonth()); //tests daysInMonth
		testOne.nextDay(); //tests nextDay
		System.out.println(testOne);

		//tests for second Date object
		Date testTwo = new Date(12, 6);
		System.out.println(testTwo.getMonth());
		System.out.println(testTwo.getDay());
		testTwo.setDate(12, 31);
		System.out.println(testTwo);
		System.out.println(testTwo.equals(testEquals));
		System.out.println(testTwo.daysInMonth());
		testTwo.nextDay();
		System.out.println(testTwo);
		
		//tests for third Date object
		Date testThree = new Date(10, 19);
		System.out.println(testThree.getMonth());
		System.out.println(testThree.getDay());
		testThree.setDate(1, 8);
		System.out.println(testThree);
		System.out.println(testThree.equals(testEquals));
		System.out.println(testThree.daysInMonth());
		testThree.nextDay();
		System.out.println(testThree);
	}
}
 