/*
 * Grading:  19/20
 * Deductions -1 (Boolean zen)
 * 
 * Look for TODO comments in your code (or search on "tasks" in the quick access window) to quickly view all of them
 * Please understand what you did wrong and avoid these problems in the future!
 * 
 * General comments:   Overall, nicely done Maggie!
 * 
 * 
 */


/*Maggie Jiang
 *12.08.18
 *Period 1 APCS
 *This program contains several methods dealing with data transformations.
 */
import java.util.Arrays;

public class Main
{
    public static final String TEXT_1 = "Star Wars: Episode VII The Force Awakens";
    public static final String TEXT_2 = "A long time ago in a galaxy far, far away..."; 
    public static final String TEXT_3 = "There's been an awakening. Have you felt it? The Dark side, and the Light.";
    public static final String TEXT_4 = "It's true. All of it. The Dark Side, the Jedi. They're real.";
    public static final String TEXT_5 = "The Force is strong in my family. My father has it. I have it. My sister has it. You have that power, too.";
    public static final String TEXT_6 = "I have to remind myself that some birds aren't meant to be caged. Their feathers are just too bright.";
    public static final String TEXT_7 = "Remember Red, hope is a good thing, maybe the best of things, and no good thing ever dies.";
    public static final String TEXT_8 = "Life is like a box of chocolates. You never know what you're gonna get.";
    public static final String TEXT_9 = "I think this is the beginning of a beautiful friendship";
    public static final String TEXT_10 = "This is your last chance. After this, there is no turning back. "
            + "You take the blue pill, the story ends, you wake up in your bed and believe whatever you want to believe. "
            + "You take the red pill, you stay in Wonderland and I show you how deep the rabbit hole goes.";
    
    public static final String[] TEXT_LIST = {TEXT_1, TEXT_2, TEXT_3, TEXT_4, TEXT_5, TEXT_6, TEXT_7, TEXT_8, TEXT_9, TEXT_10};

    public static void main(String[] args)
    {
        /* There's no need to implement anything in main for this assignment
         * You can use the unit test to validate your work
         */
    }
    
    //returns length of the longest string in the array
    public static int maxWordLength(String[] list) {
    	int length = 0; //start at minimum length to account for all cases
    	for(String elem : list) { //goes through every element and updates max length if the element longer than current length
    		if (elem.length()>length) {
    			length = elem.length();
    		}
    	}
    	return length;
    }
    
    //returns the tokens of the input in a new array
    public static String[] splitString(String sentence) {
  
    	sentence=sentence.trim(); //eliminates beginning and end spaces if applicable
    	
    	String [] list = new String[0]; //creates the array
    	
    	if(sentence.equals("")) { //for empty strings
    		return list;
    	}
    	
    	while(true) {
    		list = Arrays.copyOf(list, list.length+1); //creates a new space in the array
    		
    		if(sentence.indexOf(" ")==-1) { //if it's the last token
    			list[list.length-1]=sentence; //store this last token in the new/final space
    			break; //ends loop
    		}
    		
    		else { //if there's still tokens left to be counted
    			list[list.length-1]=getFirstWord(sentence); //fills the new space in the array with first token
    			
        		sentence=skipFirstWord(sentence); //updates input string
        	
        		while(sentence.charAt(0)==' ' || sentence.charAt(sentence.length()-1)==' ') { //gets rid of extra spaces between words
        			sentence=sentence.trim();
        		}
        		
    		}
    		
    	}
    	return list;
    	
    }
    
    //returns the first token in a string input
    public static String getFirstWord(String input) {
    	input=input.trim();
    	return input.substring(0, input.indexOf(" ")); //from start to the first space
    }
    
    //returns string input - the first token
    public static String skipFirstWord(String input) { //from just past first space to end
    	input=input.trim();
    	return input.substring(input.indexOf(" ")+1);
    	
    }
    
    //returns identical tokens between two strings as a new array
    public static String[] intersectWords(String sentence1, String sentence2) {
    	String[] arr = splitString(sentence1); //places tokens of string 1 in an array
    	String[] arr2 = splitString(sentence2); //places tokens of string 2 in an array
    	
    	String[] result = new String[0]; //initial array to be returned has length 0
    	if(sentence1.equals("") && sentence2.equals("")) return result; //for empty strings
    	for (int i = 0; i<arr.length; i++){ //for each element in first array, sees if the element is equal to any element in the second array
    		
    		for (int j = 0; j<arr2.length; j++){//checks each element in second array for equality
    			
    			if (arr2[j].equals(arr[i])){ //if there is equality
    				
    				boolean unique=true;
    				for (int k=0; k<result.length; k++){ //tests to see if array to be returned already accounts for the equivalent token
    					if(arr[i].equals(result[k])) unique = false;
    				}
    				
    				//TODO: Boolean ZEN!!
    				//Also, do you see how this code is getting a bit complex?   This equality code would fit nicely in a helper method.
    				if(unique==true){ //if the discovered equivalent token isn't already in the new array
    					result = Arrays.copyOf(result, result.length+1); //adjust array to be returned to include this new token
    					result[result.length-1]=arr[i];
    				}
    				
    			}
    			
    		}
    	
    	}
    	return result;
    }
    
    //takes a list of sentences as an array of strings and tallies the number of words with different lengths up to the length of the longest word in the array 
    //TODO: Note:  the [0] index in the return array should always contain 0.
    public static int[] wordLengthsTally(String[] sentenceList) {
    	int[] counter = new int[1]; //creates the array to be returned
    	
    	for(int i = 0; i<sentenceList.length; i++){ //for each element/sentence in the passed in array
    		String[] initial = splitString(sentenceList[i]); //tokenizes the element/sentence by putting it into a new array
    		
    		if(maxWordLength(initial)>=counter.length){ //adjusts the size of the array to be returned to fit all the possible word lengths
    			counter = Arrays.copyOf(counter, maxWordLength(initial)+1);
    		
    		}
    		for(int j = 0; j<initial.length; j++){ //tallying happens here
    			initial[j].trim();
    			counter[initial[j].length()]++;
    		}
    	}
    	
    	for(int i = 0; i<counter.length; i++){ //prints results as the pdf says to do
    		System.out.println("Number of words of length " + i + ": " + counter[i]);
    	}
    	
    	return counter;
    }
    
    public static String[] generateNGrams(String text, int size)  {
    	return new String[5];
    }
    
    

}
