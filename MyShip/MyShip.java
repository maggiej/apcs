import java.awt.Color;

import ihs.apcs.spacebattle.*;
import ihs.apcs.spacebattle.commands.*;

public class MyShip extends BasicSpaceship {
	
	private int worldWidth;
	private int worldHeight;
	Point middle;
	 
    @Override
    public RegistrationData registerShip(int numImages, int worldWidth, int worldHeight)
    {
    	this.worldWidth=worldWidth;
    	this.worldHeight=worldHeight;
    	middle=new Point(worldWidth/2, worldHeight/2);
        return new RegistrationData("Hello", new Color(255, 255, 255), 0);
    }
    
    private enum MainStates {
    	  GOTOSTARTPOINT,
    	  DRAWLINE1,
    	  DRAWLINE2
    	  //TODO: What other states are needed here?  Also, have a DONE State to know when you are done drawing...
    	 }
    	 
    	 private enum DrawLineStates {
    	  POINTINCORRECTDIRECTION, // If needed.
    	  DEPLOYSTARTBEACON,
    	  THRUST,
    	  COAST,
    	  //TODO: What other states are needed?
    	 }
    	 
    	 private MainStates currentState = MAINSTATES.GOTOSTARTPOINT;
    	 private DrawLineStates currentDrawState;
    	 
    	 private Point startPoint = new Point(400,400);
    	 private ObjectStatus status = new ObjectStatus();
    	 public ShipCommand getNextCommand(BasicEnvironment env) {
    	  
    	  // Store this object locally - It has a lot of really handy stuff.
    	  ObjectStatus ship = env.getShipStatus();
    	  status = env.getShipStatus();
    	  // TIP!
    	  // Print out your current position, orientation, and speed using println
    	  // Will greatly help you debug your code.
    	  // Also, before you return any object, use a println statement to return what you are doing
    	  
    	  System.out.println("Current State is: " + currentState);
    	  if (currentState == MainStates.GOTOSTARTPOINT) {
    	   
    	   // TODO: One addition:   You should have code like this in the appropriate place
    	   // to advance your state machine to the next part when you are at the point.
    	   if (ship.getPosition().getDistanceTo(startPoint) < 20) {
    	    System.out.println("Am now at the point - Will start drawing");
    	    currentState = MainStates.DRAWLINE1;
    	    currentDrawState = DrawLineStates.POINTINCORRECTDIRECTION;
    	    return new BrakeCommand(0);
    	   }   
    	   
    	   /*
    	    * TODO: In here would go all the logic you have to goto a particular point.
    	    * This is your code basically from find the middle.
    	    * You could start in the center, but it's probably better if you pick a point somewhere else
    	    *
    	    */
    	   
    	  }
    	  
    	  if (currentState == MainStates.DRAWLINE1) {
    	   System.out.println("Current DrawState is: " + currentDrawState);
    	   if (currentDrawState == DrawLineStates.POINTINCORRECTDIRECTION) {
    	    // TODO: Rotate to point to the right.   Only have to do this
    	    // for the first line.
    	    currentDrawState = DrawLineStates.DEPLOYSTARTBEACON; // Set next state
    	    return new RotateCommand(-1 * ship.getOrientation());
    	   }
    	   
    	   if (currentDrawState == DrawLineStates.DEPLOYSTARTBEACON) {
    	    currentDrawState = DrawLineStates.THRUST; // set next state
    	    return new DeployLaserBeaconCommand();
    	   }
    	   
    	   /*
    	    * TODO: Handle all other DrawLineStates.   On the last state, set currentState to be the next
    	    * appropriate state.
    	    *
    	    */
    	  }
    	  
    	  /*
    	   * TODO: Handle all the other MainStates.  As you go through this, you may see
    	   * lots of duplicate code - Refactor as appropriate!
    	   *
    	   */
    	  
    	  // Should never get called b/c your states above should handle everything.
    	  System.out.println("ERROR: This shouldn't ever get called");
    	  return new IdleCommand(0.1);
    	 }
    	 
    	}