//Maggie Jiang
//03.01.19
//Period 1 APCS
//This program tests the code by creating a vehicle object and using some of its methods.

public class Main {
	public static void main(String[] args){
		Vehicle lanita = new Vehicle(20000, 2500, .1); //new vehicle
		
		System.out.println("Purchase price: " + lanita.getListPrice()); //list price
		System.out.println("List price: " + lanita.purchasePrice()); //purchase price
		
		lanita.changeMarkup(1000); //markup changed
		
		System.out.println("Purchase price: " + lanita.getListPrice());//new list price
		System.out.println("List price: " + lanita.purchasePrice());//new purchase price
	}
}
