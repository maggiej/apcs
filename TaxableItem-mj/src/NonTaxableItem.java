//Maggie Jiang
//03.01.19
//Period 1 APCS
//This class defines the state and behavior of NonTaxableItems.

public class NonTaxableItem implements Item { //can-do
	double listPrice; //field
	
	public NonTaxableItem(double listPrice) { //constructor
		this.listPrice=listPrice;
	}
	
	public double purchasePrice() { //returns the purchase price which is the listprice
		return listPrice;
	}
}
