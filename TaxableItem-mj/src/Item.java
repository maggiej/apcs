//Maggie Jiang
//03.01.19
//Period 1 APCS
//This interface defines the abstract methods for Items

public interface Item {
	double purchasePrice(); //Items have a purchasePrice method.
}
