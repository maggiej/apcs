//Maggie Jiang
//03.01.19
//Period 1 APCS
//This class defines the state and behavior of TaxableItems.

public abstract class TaxableItem implements Item{ //a TaxableItem is-a Item
	
	private double taxRate; //field
	
	public abstract double getListPrice(); //abstract method to get list price.
	
	public TaxableItem(double rate) { //constructor with tax rate.
		taxRate=rate;
	}
	
	public double purchasePrice(){ //returns the purchase price, which is the list price plus the tax.
		return taxRate*getListPrice() + getListPrice();
	}

}
