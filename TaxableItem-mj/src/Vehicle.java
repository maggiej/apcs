/*
 * Grading:  7/7
 * Deductions:
 * 	Project Name: Ok
 * 	Comments: Ok
 * 	Authorship: Ok
 * 	Style:
 * 
 * Implementation:
 * 	Item/TaxableItem (PurchasePrice): Ok 
 *	TaxableItem: Ok
 *	Vehicle: Ok
 * 	TestCode: Ok
 
 * Look for TODO comments in your code (or search on "tasks" in the quick access window) to quickly view all of them
 * Please understand what you did wrong and avoid these problems in the future!
 * 
 * General comments:   
 * 
 */


//Maggie Jiang
//03.01.19
//Period 1 APCS
//This class defines the state and behavior of Vehicle objects.

public class Vehicle extends TaxableItem{ //a vehicle is-a taxableitem
	
	//fields
	private double dealerCost;
	private double dealerMarkup;
	private double listPrice;
	
	//constructor. 
	public Vehicle(double dealerCost, double dealerMarkup, double taxRate) {
		super(taxRate);
		this.dealerCost=dealerCost;
		this.dealerMarkup=dealerMarkup;
	}
	
	//returns the list price. the list price is the dealermarkup + dealercost.
	public double getListPrice(){
		//TODO: The field listPrice doesn't really buy you much here - Just return the two values...
		listPrice=dealerMarkup+dealerCost;
		return listPrice;
	}
	
	//changes the dealermarkup
	public void changeMarkup(double newMarkup) {
		this.dealerMarkup=newMarkup;
	}
}
