//TODO Score: 6/6 Don't tell anyone, but you are my favorite student!

//Maggie Jiang
//AP CS Period 1
//Single for loops project

public class SingleForLoops {
	public static void main(String[] args) {
		
		//TODO Well done Maggie!
		//the following for loop prints out the numbers from 1 to 10 backwards then goes to next line
		for (int i = 10; i >=1; i--) { //body repeats ten times, value of i decreases by 1 each time
			System.out.print(i + " "); //prints out the current value of i and a space
		}
		System.out.println();
		
		//the following for loop adds the numbers from 1 to 10 together then goes to next line
		int sum = 0;
		for (int j = 1; j <=10; j++) { //j increases by 1 ten times
			sum = j + sum; //adds the current value of j to the current value of sum and stores that value for sum
		}
		System.out.println("The sum of the numbers from 1 to 10 is " + sum);
		
		//the following for loop adds the even numbers from 1 to 20 together
		int evenSum = 0;
		for (int k = 0; k<=20; k+=2) { //k increases by 2 until it reaches 20
			evenSum = k + evenSum; //adds current value of k to current value of evenSum and stores that value for evenSum
		}
		System.out.println("The sum of the even numbers from 1 to 20 is " + evenSum);
		
	}
}
