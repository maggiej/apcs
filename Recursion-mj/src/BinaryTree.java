import java.util.*;
 
/* 
Recursion is often used in what are called 'recursive data structures'

A recursive data structure is a data structure (class) that refers to 
itself in in its fields.

A Binary Tree is a data structure that represents a node that has the 
following.  Our binary tree are used to hold a key and a value so that
given a key value we can look it up quickly and find the corresponding
value.  Here is the essential methods.

class  BinaryTree {
    public BinaryTree() { ... } 
    public void put(String key, String value) { ... }
    public String get(String key) { ... }
}

Basically put adds key-value pairs to the table, and 'get' fetches them.
Here is a sample use 

static void main(String[] args) {
    BinaryTree table = new BinaryTree();	// Create an empty table

    // put some key-value pairs into the table 
    table.put("Key5", "Value5");
    table.put("Key8", "Value8");
    table.put("Key3", "Value3");
    table.put("Key1", "Value1");
`
	// And new fetch them
    assert(table.get("Key8").compareTo("Value8") == 0);
    assert(table.get("Key3").compareTo("Value3") == 0);

	// It returns null if it is not present 
    assert(table.get("KeyX" == null));
}

We will implement this key-value lookup by having a binary tree.  
Each node in the tree will have

	1) The key and value strings
	2) A 'left' child which is another binary tree with a key <= than the current one
	3) A 'right' child which is another binary tree with a key >= than the current one
	
Notice that the a node has fields that point to other nodes this is
what makes it recursive. The child nodes are OPTIONAL and thus may be null.

To support an 'empty' table, The 'key' for the top most node may be null.
This is how you represent a empty table.

The 'put' method will search the table for the same key.  If it exists in
the table the value is updated.  If not it is added to the table. 

Here is a sample use 

static void main(String[] args) {
    
    BinaryTree table = new BinaryTree();	// Create an empty table

        // put some key-value pairs into the table 
    table.put("Key5", "Value5");
    table.put("Key8", "Value8");
    table.put("Key3", "Value3");
    table.put("Key1", "Value1");
`
	// And new fetch them
    assert(table.get("Key8").compareTo("Value8") == 0);
    assert(table.get("Key3").compareTo("Value3") == 0);

	// It returns null if it is not present 
    assert(table.get("KeyX" == null));
}

Assignment
1) REQUIRED: Implement the basic constructor, put, get operations

	Make sure that you comment each of the methods as well as the class.
	The comment before methods/class describe HOW TO USE IT comments
	in the body describe HOW IT WORKS.  
	
	It is illegal to try to put a null key into the table (why what goes wrong?)
	Thus add a precondition assert on the 'put' method for this.
	
	It is also a bad idea if value is null, because that is what 'get' returns
	when there is nothing in the table (thus there is ambiguity) 
	Thus add a precondition assert on the 'put' method to insure that this does not happen.
	
	Add postcondition asserts on get (what do you know about the returned value)
	
2) REQUIRED: Implement JUnit tests for BinaryTree.  

	Add Tests that implement 100% code coverage   
	You will need to justify that you achieved this.  
	Hints 
	
	  1) You will probably need different sized trees (with 0, 1, 2, 3 elements)
	  2) You will want to test both success finding things as well as failure
	  3) At least one test should make 'put' REPLACE a value (not add one).  
	  4) Don't skimp, tests are cheap
	  5) Make them REGULAR and UNIFORM.  
	 
3) REQUIRED: Implement the String toString(String indent) method  

	The call 'table.toString("*       ")' produces the following string where
	'table' is the table in the example above (ignore the spaces before the *)
	
	 *       Key5 -> Value5
	 *        Key3 -> Value3
	 *          Key1 -> Value1
	 *        Key8 -> Value8
	
	That is each line has a key-value pair, and has 'indent' in front,
	The two children then are printed (and all all their children).  
	If the children are null they are simply skipped.
	
	calling toString on the empty table will return the empty string.  
	
	As always comment the method, use pre conditions easy post-conditions.  
	Also implement a test for toString.  Again I want 100% code coverage. 

	hint: you can use + to break up newlines in your code.  For example
	
	   String expectedOuput = 
	       "  Key5 -> Value5\n" + 
	       "    Key3 -> Value3\n"; 
	       
	This will make your code more readable. 
	
	hint: you can add more asserts to existing tests.  That way you are
	effectively reusing the 'setup' code.  

4) REQUIRED  implement the method: int count() 

    This method returns the number of key-value pairs in the table. 
    Thus an empty table should return 0, and table in the example above
    will return 4.  
    
    As always, comments, asserts, tests, coverage.  
    
5) EXTRA implement the method: int depth() 

	This method returns the maximum depth of the tree.  This is
	the number of 'left' or 'right' pointers you follow from the
	root to a node.   
	
	As always, comments, asserts, tests, coverage. 
	
6) EXTRA implement the method: List<String> keys()

	This method returns all the keys in the table as a List.
	This list is naturally in SORTED order.  
	You must assert that the list is sorted.   
	
	Hint: Your implementation should only make ONE ArrayList.
	To do this you will need a private helper method 
	
	   List<String> getKeys(List<String> listToUpdate)
	
	This helper is passed a list, which it adds element to
	and returns.  Thus one list is passed around and filled
	up in order.   
	
	You will need to assert that the returned value is sorted.  
	
	Hint: to assert that the list is sorted  create the private helper function 
	
		static boolean isSorted(List<String> list)

    Notice that this routine does not is a general utility
    and has nothing really to do with BinaryTree.  You implement
    it just because you need it for your assert.  
    
    As always, comments, asserts, tests, coverage. 
    
7) EXTRA implement List<String> keys(String min, String max)
	
	This is like keys() except it only returns the keys that
	are in the range from min (inclusive) to max (exclusive).
	You should implement this so that it is efficent when the
	range is small and the list is large (that is don't add
	everything just to remove them later).  
	
	As always, comments, asserts, tests, coverage. 
	
8) EXTRA show that loading random values will tend to produce a 
    balanced tree. 
   
    Create a method: static BinaryTree createRandomTree(int n)
    
    This uses Math.Random to generate a number N between 0 1,  
    Make key 'Key' + n and value 'Value + n' and add it to the
    table.
    
    Then compute its count and depth.  
   
    The cost of the 'get' method is proportional to depth.  
    
    Compute and print the depth of trees of size 1024 (a depth
    of 10 is perfectly balanced) and  16384 (a depth of 14 is
    perfectly balanced). 

9) EXTRA: write the method: BinaryTree rebalence()

 	This routine takes the current tree and returns a new BinaryTree
 	that is perfectly balanced (what can you assert about its depth?)
 	
 	Hint: don't try to be smart.  Simply use the 'keys' method to 
 	get all the keys and then find the middle key in this list and
 	add that to a brand new list (and its corresponding value) first, 
 	Then call yourself recursively to insert the 1/4 and 3/4 positions.
 	(see text on binary search, this is what we are doing).  
 	
 	There is not a lot of code in this method if it long you are 
 	doing it wrong!
 	
 	As always, comments, asserts, tests, coverage. 
****************************************************************/

public class BinaryTree {
	public BinaryTree() {
		// TODO implement
	}

	public void put(String key, String value) {
		// TODO implement
	}

	public String get(String key)
	{
		return null; // TODO remove and implement
	}

	public String toString(String indent)
	{
		return null; // TODO remove and implement
	}
	
	public int count()
	{
		return 0; // TODO remove and implement
	}

	// fields
	String key;
	String value;
	BinaryTree left;
	BinaryTree right;
}

