/*
 * Grading:  16/16
 * Deductions:  (See below)
 * 
 * Look for TODO comments in your code (or search on "tasks" in the quick access window) to quickly view all of them
 * For this project - Please correct (or address) and TODO items for the next part.
 * 
 * General comments:
 *  Coding/Style/Naming: 
 *  Lateness of Submission:  
 *  Assertion:
 *  FactorialRec:
 *  FactorialLoop: 
 *  Factorial tests: 
 *  FactiorialRec Logging:
 *  RepeatString:
 *  RepeatStringDQ:	
 *  RepeatString tests:	
 *  Extra Credit: Did not see any - let me know if I'm wrong.
 *  
 * Other comments:	Nicely done!
 * 
 *  
 */

//maggie jiang
//3.31.19
//period 1 apcs
//this file completes 8 exercises regarding recursion

public class Main {
	
	public static void main(String[] args)
	{		
		System.out.println("TODO Implement main");
		//divideTwoNumbers(1, 0); instructions said to comment it out
		factorialRec(10, ""); //testing the indent thing with the factorial method
		repeatString("*", 32, ""); //exercise #6
		repeatStringDQ("*", 32, ""); //exercise #7
		System.out.println(repeatStringDQ("*", 32, "")); //#7
		System.out.println(repeatStringDQ("*", 9, "")); //testing an odd number with DQ method
	}
	
	/* 
	 * ****************************************************************
	#Asserts
	Asserts are helpful debugging tools that developers often add their code. The idea is that if your code contains any assumptions, you can assert that those are true.
	If they are not, your code stops running and fails with a helpful message.
	
	The syntax looks like this:
	assert <boolean condition> : <message>
	
	The key part is that the condition is asserting what is true (if this condition is false, the assert fails)
	
	e.g.
	assert str.length() != 0 : "String should not be empty"
	
	For asserts to work, you must enable asserts when you run your program.  To do this,
	1) goto run configurations (under the run dropdown)
	2) Go into the "arguments" tab" 
	3) Add -ea to the VM arguments textbox (-ea stands for -enableassertions)
	4) You can then run your code and verify it works.	
		****************************************************************	
	*/
	

	// divide two numbers
	/*
	public static int divideTwoNumbers(int a, int b) {
		/* #1 
		 * a) Implement this function and add an assert that fires if b is zero.
		 * b) Then add a call to this function in main
		 * c) Enable assertions for this project
		 * d) Run main - Verify your assertion fires.
		 * e) Comment out this method when done...
		 *  
		 */
		/*assert b != 0 : "Cannot divide by zero"; //the assert that we're supposed to add
		return a/b;
		
	} */
	
	/*
	***********************************************************************
	 Introduction to Recursion 
	 
	 Factorial * the product of all whole numbers less than a given number  
	 
	  	factorial(5) = 5 * 4 * 3 * 2 * 1 == 120 
	 
	 How do we define this precisely?   
	 Sometimes the definitions are naturally recursive.   For example one definition is 
	 
	     factorial(N) = N * (N-1) * (N - 2) * (N-3) ... * 3 * 2, * 1
	 
	 But you can also do it this way
	 
	     factorial(0) = 1 
	     factorial(N) = N * factorial(N-1)
	     
	 Notice you define it in terms of itself, but a SMALLER version of it.  
	 	 
	*************************************************************
	*/
	
	//	#2 REQUIRED Implement static int factorialRec(int n) using recursion     
	//		Is there something you should be asserting?  
	
	//after exercise 5 is done
	public static int factorialRec(int n, String indent) {
		assert n>=0 : "Cannot be negative"; //assert
		System.out.printf("%scalling factorialRec(%s) {\n", indent, n); //indent thing
		int ret;
		if (n==0) { //base case
			ret = 1;
		}else { //recursive case
			ret = n*factorialRec(n-1, indent + "  "); //return variable
		}
		System.out.printf("%s} factorialRec(%s) returns %s\n", indent, n, ret); //indent thing
		return ret;
	}
	
	//original method for #2
	/*public static int factorialRec(int n){
	 assert n>=0 : "Cannot be negative";
	 if(n==0){ //base case
	 return 1;
	 }else{
	 return n*factorialRec(n-1); //recursive case
	 }
	 }
	 */

	// #3 Implement static int factorialLoop(int n) using a for loop.  
	public static int factorialLoop(int n) {
		assert n>=0 : "Cannot be negative"; //assert
		int product = 1;
		for(int i = n; i>0; i--) { //factorial loop
			product*=i;
		}
		return product;
	}
	
	// #4 REQUIRED Create a JUnit test that tests that the two ways of doing 
    //		factorial give the same answer for N = 0 through N = 10;
	//see tests file		
	
	/*
	 * **********************************************************************
	How to debug recursive programs

	Just like buggy programs can have infinite loops, buggy
	recursion can have infinite recursion.   However recursion
	uses memory on each call, and thus when recursion goes
	infinite you run out of something call stack space and
	you get a 'StackOverflow' exception.   Then you typically
	get a VERY LONG call stack, which is typically not very 
	helpful.  

	We have taught you that in general logging at critical 
	points is a good way to understand your program this is 
	especially true for recursive programs.

	The critical points to log when using recursion are
	    1) On entering the recursive function.  Print out the args
	    2) On leaving the function print out the return value.  

	It VERY valuable to print things that are indented so
	that 'child' calls are indented more than their parent.
	This requires that you pass another argument (the amount
	of indenting) and increase it before making sub-calls.
	For example here is a recursive method that repeats a 
	string n times (e.g  repeatString("*", 5) === "*****")

	    public static String repeatString(String str, int n)
		{
			if (n == 0)
				return "";
			return repeatString(str, n-1) + str;
		}

	Here is how it looks when instrumented 

	    public static String repeatString(String str, int n, String indent)
		{
			System.out.printf("%scalling repeatString(%s, %d) {\n", indent, str, n);
			String ret;
			if (n == 0)
				ret = "";
			else
				ret = repeatString(str, n-1, indent + "  ") + str;
				
			System.out.printf("%s} repeatString(%s, %d) returns %s\n", indent, str, n, ret);
			return ret;	
		}

	Here we are using the 'printf' method, which is like println, but you can have %s
	and %d in the string which will get replaced by strings and integers in arguments that
	follow.   This is typically clearer than using concatenation for complex strings 

	Notice that you had to add an 'indent' argument to the method and you had to 
	change the code so that you only return at the end of the method (so you can print
	something before you leave).   Notice that the recursive call to repeatString we add
	two spaces to the indent.   Finally we use { at the first log message and } at the
	last so that we get matching {} in the log.  

	The result of running repeatString("*", 5) is now 

			calling repeatString(*, 5) {
			  calling repeatString(*, 4) {
			    calling repeatString(*, 3) {
			      calling repeatString(*, 2) {
			        calling repeatString(*, 1) {
			          calling repeatString(*, 0) {
			          } repeatString(*, 0) returns 
			        } repeatString(*, 1) returns *
			      } repeatString(*, 2) returns **
			    } repeatString(*, 3) returns ***
			  } repeatString(*, 4) returns ****
			} repeatString(*, 5) returns *****

	Notice that this trace is what you should be doing when you do 'mystery' traces 
	by hand.   It is a REALLY good summary of what is going on.


	   
	   	**********************************************************************
	
	*/	
	
	// 	#5 REQUIRED Change static int factorialRec(int n) so that it logs with indenting.
	// 		Read carefully the directions on what you need to do.  
	//		1) Add an indent parameter
	//		2) Modify the printf statements appropriately (keep in mind that the numbers and types of the "%" formatting symbols *must* match the arguments (starting at the 2nd parameter)
	//		3) Modify the structure of your code so that you have a "ret" variable and you can then log what you return before you return it.
	//		Note:  You will have to update your unit test as well.			
    //		Based on the log, how many times does 'factorialRec' get called for a value of N? 
	
	//factorialRec gets called N+1 times for a given value of N.
		
	
	// #6 REQUIRED The repeatString code has been pasted in below - run it for repeatString("*", 32) (add a call in main)  
	//		How many times was repeatString called?	
	//		repeatString was called 33 times
    public static String repeatString(String str, int n, String indent)
	{
		System.out.printf("%scalling repeatString(%s, %d) {\n", indent, str, n);
		String ret;
		if (n == 0)
			ret = "";
		else
			ret = repeatString(str, n-1, indent + "  ") + str;
			
		System.out.printf("%s} repeatString(%s, %d) returns %s\n", indent, str, n, ret);
		return ret;	
	}
    
    /*
    ****************************************************************************
    Divide and Conquer

    The examples so far are what are called 'linear' recursion because the traces
    look like a line (each call calling the same method with one smaller argument)

    Generally however linear recursion is not valuable in practice because it can easily
    be turned into a loop, and recursion slower than the loop (and uses more memory).  

    However if the problem can be expressed as a two roughly equally sized smaller
    problems, then you you get VERY efficient algorithms.  This is where recursion
    is used in the real world.  

    In the case of the 'repeatString' we notice that if N was 8 we can write

    	repeatString("*", 8) == repeatString("*", 4) + repeatString("*", 4)

    Notice that the string is the SAME, thus you can write this as 

    	String s4 = repeatString("*", 4); 
    	repeatString("*", 8) == s4 + s4;

    That a string of 8 * is the same as two strings of 4 * concatenated together.  

    Notice we have expressed the problem in terms of a problem about half as big.
    This is divide and conquer!

    Now this worked great because 8 was even.  What if it was 9 instead? No problem

    	repeatString("*", 9) == s4 + s4 + "*";
    	
    	
    Thus we have a way of break a problem down no mater if it is even or odd 
    (remember N % 2 == 0 only if N is even).   
    	


    	Look at the log, does it make sense?   How is it different from the first log?
    	How many times is repeatString called?   


    ********************************************************************************    
    */
    
    // #7 REQUIRED copy repeatString and rename it to repeatStringDQ (for divide and conquer).
    //		Be sure you change the recursive call to repeatStringDQ as well.
    //		Then change your repeatString implementation make it divide and conquer.    
    //		Leave the indented logging in place.   You should only have to change several 
    //		lines of code (most of it is unchanged). Run it on repeatStringDQ("*", 32, "")
    
    public static String repeatStringDQ(String str, int n, String indent)
	{
    	boolean even= n%2==0; //boolean to see if n is even
    	n/=2; //do the repeat string method half the number of times
		System.out.printf("%scalling repeatStringDQ(%s, %d) {\n", indent, str, n);
		String ret;
		if (n == 0) {
			ret = "";
		}else {
			ret = repeatStringDQ(str, n-1, indent + "  ") + str;
		}	
		System.out.printf("%s} repeatStringDQ(%s, %d) returns %s\n", indent, str, n, ret);
		ret+=ret; //then concatenate the string with itself
		if (!even) ret+=str; //add an extra str if it was odd to begin with
		return ret;	
	}
    
    // #8 REQUIRED make a JUnit test that tests that repeatString("*", n) and 
    //		repeatStringDQ("*", n) are the same for n = 0 to 10.  
    //see other file
    
    // DONE?  Once you have done 1-8 above, investigate some of the other files and try some of the activities there...
    //	a) AdvancedScenarios - Advanced versions of the above (and some variations)
    //	b) BinaryTree - Very cool usage of recursion to store data in a "tree"
    //	c) FileSystem	- using recursion to solve a real problem (by examining file systems)
    
    
}
