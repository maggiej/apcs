import java.util.function.Consumer;
 
/**
 * PlotTime is a class for making tables of how long various operations take.  
 */
public class PlotTime {
	/**
	 * Produces a table, measuring the time it takes action(N) to run for various values of N.  
	 * It also shows how many times the PlotTime.inc() method has been called. by Action.  
	 * @param title - the title is printed before the table 
	 * @param action - action is code to measure.  It takes a parameter N.
	 * @param startN - The first N passes to Action
	 * @param maxN - The maximum value of N passed to Action. 
	 * @param mult - After measuring action(N) increase N by this factor (thus) NPlus1 = N * mult
	 * @param maxSec - If action(N) takes longer than this time, the plotting also stops.  
	 */
	public static void Plot(String title, Consumer<Integer> action, int startN, int maxN, double mult, int maxSec)
	{		
		System.out.println();
		System.out.println(title);
		System.out.println("--------------------------------------------------------------------------------------");	
		double lastDuration = 0;
		long lastCount = 0;
		double factor = 1;
		int lastNum = 0;
		for(;;)
		{
			int num = (int) (startN * factor);
			factor = factor * mult;
			
			if (lastNum == num)
				continue;
			lastNum = num;
			
			if (maxN < num)
				break;

			count = 0;
			Stopwatch sw = new Stopwatch();
			
			action.accept(num);
			
			double durationSec = sw.getElapsedSec();
			
			double ratio = 0;
			if (lastDuration != 0)
				ratio = durationSec / lastDuration;
			lastDuration = durationSec;
			System.out.format("    N: %10d Time: %10.3f Sec  Time Ratio: %6.2f", num, durationSec, ratio);
			
			if (count != 0)
			{
				double countRatio = 0;
				if (lastCount != 0)
					countRatio = ((double) count) / lastCount;
				lastCount = count;
				System.out.format(" Count: %12d CountRatio %6.2f", count, countRatio);
			}
			System.out.println();
			
			if (maxSec <= durationSec)
				break;
		}
		System.out.println("-------------------------------------------------------------------------------------");	
	}

	public static void Plot(String title, Consumer<Integer> action, int maxN)
	{
		Plot(title, action, 32, maxN, 2, 10);
	}
	
	public static void Plot(String title, Consumer<Integer> action)
	{
		Plot(title, action, 100000000);
	}
	
	/**
	 * The Plot method also will display how many times Plot.inc() is called.  
	 * TYpically this is put in the 'innermost' operation (to compute complexity).  
	 */
	public static void inc()
	{
		inc(1);
	}
	
	/**
	 * inc(n) is equivalent to calling inc() N times.  
	 * @param n
	 */
	public static void inc(int n)
	{
		count += n;
	}
	
	// private fields 
	static long count;
}
