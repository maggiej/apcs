import java.io.*;
 
/*
Review/Introduction to file PATH names 
    All files on a computer have a 'path' that tells you how to find it.  

    On Windows a file looks like
    
        C:\dir1\dir2\fileName

    The C is called the 'Drive' and is only one letter.  
    The \ represent directories (places to put files.
    Notice that directories can have other directories. 

    In Unix paths look like

	/dir1/dir2/fileName

    This concept of a place to put things' is called a 'namespace' and namespaces that
    can have sub-namespaces is called a hierarchical namespace.   

    Notice that every directory/file (except for the root) has EXACTLY one 'container' 
    (also called Parent) directory.   

    A data structure (like a directory) where each node has no more than one parent is called a 'tree'.

The java.io.File class 

    The File class is a class that represets either a file or a directory on the computer.   

	Constructors for java.io.File
	    File(String pathname)
		    Creates a new File instance by converting the given pathname string into an abstract pathname.
	
	Methods for java.io.File
	
	    boolean isDirectory() 
		    eturns true if the file is a directory (otherwise it is a file)
	
	    int length() 
		    returns the number of bytes in the file (or in the representation of the directory)
	
	    File[] listFiles()
		    returns all the files in a directory.
	
		String getPath()
		    returns the sequence of directories and the name of the file needed to
		    find this file.   This MIGHT be from the root of the machine (see 
		    getCannonicalPath()) but it also might be relative to the current directory.  
		    (see Current Directory section below) 
	
	    String getName()
		    returns the name of the file IN THE DIRECTORY (not the complete path)
	
	    File getParentFile()
		    returns the directory in which the file or directory resides.
	
	    String getCannonicalPath()
		    returns full path from the root for the machine.  (always starts with C:\).  
 
Current Directory Concept
	Every application on the machine is given a 'currnet' directory.   This can be changed
	over the lifetime of the app but at any particular point in time there is only one 
	for any given running application (call a process).  
	
	File names that look like root of the file system (e.g. c:\.... on windows /... on linux)
	are called absolute file names.   File names that don't begin this way (for example
	hello.txt or x\y\x\hello.text are call relative paths.  and are relative to the
	current directory.  
	
	There are two special names (for both windows and unix) 
	
	. - represents this directory.   Thus  .\X and X are the same path.
	.. - represents the parent of this directory.   Thus X\..\Y is the same as Y (assuming X exists). 

Assignments
1) REQUIRED: Write code in 'main' to find out the full canonical path to the current 
   directory is of the program that gets run when you click 'Run' in eclipse.   
   
   Hint you can do this by referring to the current directory "." and make a File object
      out of it, and then calling getCannonicalPath().
   Hint: see throws clauses above
   Hint the solution is 2 or fewer lines of code.  
   
   What is the current directory where your program runs?

2) REQUIRED: in the FileSystem class create static void printAllFiles(String dirOrFilePath)

	This method will list the full paths of all the files and directories in a given 
	file path.   Use it to print all the files/directories in the current directory
	for your application (which is the directory for your RealCode project)  Here
	is what it did on my machine. 

		.
		.\.classpath
		.\.project
		.\.settings
		.\.settings\org.eclipse.jdt.core.prefs
		.\bin
		.\bin\BinaryTree.class
		.\bin\FileSystem.class
		.\bin\LinkedList.class
		.\bin\Main.class
		.\bin\PlotTime.class
		.\bin\Recursion1.class
		.\bin\Stopwatch.class
		.\src
		.\src\BinaryTree.java
		.\src\FileSystem.java
		.\src\LinkedList.java
		.\src\Main.java
		.\src\PlotTime.java
		.\src\Recursion1.java
		.\src\Stopwatch.java

     Notice that all directories (like ., bin and src) and each directory
     is followed by files in that directory.   
     
     Hint: First create a method 
     
     static void printAllFiles(File file)
 
 	 that does the operation on a File object, and then make the desired
 	 method 
 	 
 	 static void printAllFiles(string path)
 	 
 	 that simply uses the File(String) constructor to make a new File object
 	 out of the given path.
 	 
 	 
3) REQUIRED: in the FileSystem class create: static int sizeOfAllFiles(String dirOrFilePath)

     this method will return the sum of the length of all files (but not directories)
     in the given directory (if it is a file, it simply returns its size.  

     Hint: Like in printAllFiles, make a method 
     
     static int sizeOfAllFiles(File dirOrFile)
     
     Use your routine to print out the size of all the files in the current directory
     of your app.  
     
4) REQUIRED in the FileSystem class create: static void listRecursively(String dirOrFilePath)
	
	 This will create a 'tree strutured output' with the size of the file and the name
	 of the file on each line, and indented to represent the nesting.  Here is the
	 output from my machine
	 
		DIR: .
		    FILE: .classpath                          301
		    FILE: .project                            384
		    DIR: .settings
		        FILE: org.eclipse.jdt.core.prefs          598
		    DIR: bin
		        FILE: BinaryTree.class                    741
		        FILE: FileSystem.class                   1793
		        FILE: LinkedList.class                    371
		        FILE: Main.class                         2300
		        FILE: PlotTime.class                     2896
		        FILE: Recursion1.class                    373
		        FILE: Stopwatch.class                     592
		    DIR: src
		        FILE: BinaryTree.java                    8504
		        FILE: FileSystem.java                    6747
		        FILE: LinkedList.java                     179
		        FILE: Main.java                           919
		        FILE: PlotTime.java                      2783
		        FILE: Recursion1.java                     190
		        FILE: Stopwatch.java                      721
	
	  Notice that you print a line per item.  If the item is a directory
	  you immediately follow it with all the items in that directory
	  but they are all indented by 4 spaces.   If the Item is a file
	  it also prints the size of the file.   
	  
	  Hint:  Like the others you will want a 'helper' method
		static void listRecursively(File file, String indent)

	  Notice however that this method use 'File' instead of a path, 
	  but it also is passed the 'indent' that you put before each line
	  When you call this for sub-items you can indent them more.  
	  
	  Hint: print strings and numbers with padding use the method
	  
	  System.out.format("%s%-s%10d", string1, string2 int1);
	  
	  The format method takes a 'formatting string' which is a string
	  with 'commands in them that begin with a %.
	  
	  An example command is %10d  This command means  
	  
	  	The 10 means that it should use no less than 10 spaces.  
	  	if the value needs less than 10 spaces, LEADING spaces
	  	are used to make it 10 spaces. 
	  	
	  	The number can be negative, in which case it uses TRAILING
	  	spaces to make the item fit.  
	  	
	  	It never trunates.  If the item is too bit, all of it is printed.
	  	
	  	The d means that it is an integer that it should be printed
	  	as a decimal number
	  	
	  	The other common format is %s which means the value is a string
	  	like the %d it can have a size specification.  

5) EXTRA - change the listRecursively method so that it prints
    the full path (rather than just the name of ever DIRECTORY (but not file)
    THus it should look like this
    
		DIR: C:\Users\vancem\OneDrive\eclipse-workspace\RealCode
		    FILE: .classpath                          301
		    FILE: .project                            384
		    DIR: C:\Users\vancem\OneDrive\eclipse-workspace\RealCode\.settings
		        FILE: org.eclipse.jdt.core.prefs          598
		    DIR: C:\Users\vancem\OneDrive\eclipse-workspace\RealCode\bin
		        FILE: BinaryTree.class                    741
		        FILE: FileSystem.class                   1822
		        FILE: LinkedList.class                    371
		        FILE: Main.class                         2300
		        FILE: PlotTime.class                     2896
		        FILE: Recursion1.class                    373
		        FILE: Stopwatch.class                     592
		    DIR: C:\Users\vancem\OneDrive\eclipse-workspace\RealCode\src
		        FILE: BinaryTree.java                    8504
		        FILE: FileSystem.java                    9430
		        FILE: LinkedList.java                     179
		        FILE: Main.java                           919
		        FILE: PlotTime.java                      2783
		        FILE: Recursion1.java                     190
		        FILE: Stopwatch.java                      721

	Hint: you will run into an error that 

	    Unhandled exception IOException
	   
    An exception is an object that represents an error in the program.   When you 
    call methods that can throw exceptions then if your method does not 'handle'
    the exception your method will ALSO throw this exception.   
		
	The java.io.File.getCanonicalName() methods can throw an IOException.  
	It is easy to forget that this might happen, and the compiler is warning you
	about this.   
	
	You can declare to the compiler that 'I know about this error it is OK'
	by using a 'throws' clause on the method that contains a call that might throw. 
	Thus you can say
	
	static void listRecursively(File file, String indent) throws IOException
	
	Which says because this method can throw and IOException it is OK if it calls
	methods that might throw this exception.
	
	Note that when you do this, the main program complains.   You have to declare
	it there as well.

6) EXTRA change listRecrusively back to only print the simple name of the directory
    but also make it print a size, which is the total (recursive) size of all files
    in that directory or any subdirectories.  

7) EXTRA Implement void printAllFiles(String dirOrFilePath, String pattern)

    This prints all the files recursively (like the previous printAllFiles) 
    however this one filters out any members that do not have the string
    'pattern' in their (simple) name.   Thus if 'pattern' was '.java' 
    it will only print out java files.  
    
8) EXTRA Implement  List<String> getAllFiles(String dirOrFilePath, String pattern) 

    which is just like printAllFiles but instead returns a ArrayList of
    strings each of which is a path that matches the pattern)
    
9) EXTRA Implement List<String> getAllFIles(String dirOrFilePath, Predicate<String> predicate) 
   
    Where Predicate<T> is defined in java.util.function.   It is a method that
    takes a string and returns a boolean.  You call it by doing  
    
    	if (predicate.test("aString"))
    
	This works just like getAllFiles but it generalizes it.   Write a main method
	that uses a Java lambda that will return all the files that have 'java' in
	their name 
*/


public class FileSystem {
	
	public static void main(String[] args)
	{
		// TODO Implement	
	}
	
	public static void printAllFiles(String dirOrFilePath)
	{
		// TODO Implement
	}
	
	public static void printAllFiles(File dirOrFile)
	{
		// TODO Implement
	}	
	
	public static int sizeOfAllFiles(String dirOrFilePath)
	{
		return 0; // TODO Implement
	}
	
	public static int sizeOfAllFiles(File dirOrFile)
	{
		return 0; // TODO Implement
	}
}
