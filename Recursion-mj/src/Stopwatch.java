 
/**
 * Stopwatch is a trivial class that allows you to measure time precisely. 
 */
public class Stopwatch {
	
	/**
	 * Create a new Stopwatch and starts it running
	 */
	public Stopwatch()
	{
		startTimeNano = System.nanoTime();
	}
		
	/**
	 * gets the current amount of time (in seconds) since the Stopwatch started running  
	 */
	public double getElapsedSec()
	{
		double stopTimeNano = System.nanoTime();
		return (stopTimeNano - startTimeNano) / 1000000000.0;
	}
	
	/**
	 * gets the current amount of time (in milliseconds) since the Stopwatch start running
	 */
	public double getElapsedMSec()
	{
		return getElapsedSec() * 1000.0;
	}
	
	// private fields
	long startTimeNano;
}
