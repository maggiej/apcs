 
/*
********************************************************************************
 With great power comes great responsibility,  Exponential complexity
 
 Factorial is not the only calculation that is naturally defined recursively.  
 There is also a sequence call fibonacci, defined this way
 
 	fibonacci(1) = 1
 	fibonacci(2) = 1
 	fibonacci(N) = fibonacci(N-1) + fibonacci(N-2)
 	
Thus the sequence is

	1, 1, 2, 3, 5, 8, 13, 21, ...

1) EXTRA define static int fibonacci(int n, String indent)  

  That computes the n'th fibonacci number.  Create logging as described above. 
 
  Are there asserts you should add?
 
  Add a unit test to test that fibonacci(6) == 8
  
  Call your fibonacci method from the main program that calls 
  fibonacci(4,"") and prints the answer.   Try it again for fibonacci(8, "") 
  
  Notice that fibonacci(8, "") calls MANY MORE times than fibonacci(4,"")
  
  Comment out the logging, and try it for fibonacci(38)?  How long does it take to compute?
  Try it for fibonacci(42)  does it even return in a reasonable period of time? 
  
  What is happening is that the number of calls is growing EXPONENTIALLY.   
  Such algorithms typically are not useful because they are so slow.  
  
*************************************************************************
Taming slowdown: Caching.  

Notice that what is slowing fibonacci down is that it is recomputing the
same values over and over again.  If it simply remembers the values, then
it could avoid that problem.

2) EXTRA - Fixing fibonacci.  

To fix fibonacci, copy it to a new method fibonacciWorker (update all calls!).
Then add a new parameter so its signature looks like
 
and then make a new method 

	static int fibonacciWorker(int n, int[] cache, String indent) 

	static int fibonacciCached(int n)

This method will 
   1) create an array of size N.
   2) call fibonacciWorker(n, array, "");
   
Notice now the worker has a place to 'remember' past values.  Change
the worker so that

	before even the logging, it checks to see if cache[n] != 0 and 
	if that is true, it simply returns it.   (we say we 'hit' the cache).
	
	When we compute a new value, we doe cache[n] = ret, to remember
	that value.  
	
Now run fibonacci(38)  (with the logging on).  Notice it is of modest size. 
 
Now try fibonacci(100) is it fast?  

***************************************************************************

3) EXTRA Implement double pow(double x, int y)

   Which returns x to the power of y.   
   
   Be sure to include assertions on any preconditions.  

   Implement it first with 'linear recursion' taking advantage 
   of the fact that pow(x, y) = pow(x, y-1) * y
   
   As always, comments, asserts tests. 

4) EXTRA Implement double powDQ(double x, int y)
 
   Which returns x to the power of y.   
   
   However this time use divide on conquer because we notice
   
   that if 
   		double = halfPower = powDQ(x, y / 2)
   then 
   		powDQ(x, y) = halfPower * halfPower
   
   As always, comments, asserts tests. 
   
5) EXTRA Implement int stringToInt(String digits)

   Which converts an decimal string (like "3423")
   to an integer using recursion.   
   
   Hint:  You need to work 'from the right' of the 
   string, converting the last digit to a integer
   then calling yourself recursively on the left
   part and combining.  
   
   As always, comments, asserts tests.  
   
6) EXTRA: Implement int stringToInt2(String digits)
   
   Which again converts a decimal string to an integer
   using recursion (no loops), however unlike the 
   previous example it does so by going decomposing
   the string left to right (it starts by converting
   the leftmost digit.  
 
   Hint: This requires another argument to be passed
   to the recursive routine.   Thus stringToInt2 is
   just a wrapper that called the 'real' recursive
   routine.   This routine is
   
   int stringToInt2Worker(int leftDigits, String rightDigits) 
   
   It returns leftDigits shifted over so they are just
   to the left of 'rightDigits'.   
   
     stringToInt2Worker(34, "67") == 3467
     stringToInt2Worker(3467, "") == 3467
   
   As always, comments, asserts tests.  
   
7) EXTRA: Implement String intToSTring(int num) 

    which converts an integer to a decimal string using recursion.  
    
    Hint, like stringToInt, it is most natural to  do this conversion 
    taking the right most digit first (the least significant digit) 
    
    Hint You can convert a integer from 0 - 9 to a character.  with the code 
    
    char digitChar = (char) ('0' + digit)
    
    You can the convert that character to a string by either appending 
    it to a existing string or by using Character.toString(char).  

***************************************************************************
 */

public class AdvancedScenarios {
	
		

}
