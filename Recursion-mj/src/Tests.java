import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
 
class Tests {

	/* 
	 * Background:  Unit tests are a critical part of writing software as they can verify that your public implementation
	 * works properly before you turn your code in (or give your code to others).   It's important to think about what types of
	 * error cases your code should catch as well as "regular cases"
	 *  
	 */
	
	/*	Here's an example of a Unit test - You can copy this and modify it to meet your needs.
	 * 	by convention, test names typically look like "test<thethingyouretesting>"
	 *  There are a variety of assert methods you can use to verify lots of various inputs (exceptions, arrays, etc..)   However, for our needs,
	 *  you can just use assertEquals which will do what you want.
	 * 
		Reminder: To call your methods, make sure to prefix your call with the class name (or import the class)
	*/ 
	
	@Test
	void testExample() {
		
		int expectedAnswer = 4;
		int actualAnswer = 2 + 2;		
		
		// You can optionally add a message as the 3rd argument
		assertEquals(expectedAnswer, actualAnswer,"Addition in java appears not to work... ;-(");		
		
	}
	
	@Test //to test that the factorial recursion method and the factorial loop method return the same thing
	void testFactorialMethods() {
		int one = Main.factorialRec(10, "");
		int two = Main.factorialLoop(10);

		assertEquals(one, two);
		
	}
	
	@Test //to test that the linear recursion method and the divide and conquer method return the same thing
	void testRepeatStringMethods(){
		for(int i =0; i<=10; i++){
			String one = Main.repeatString("*", i, "");
			String two = Main.repeatStringDQ("*", i, "");
			
			assertEquals(one, two);
		}
	}
	

}
