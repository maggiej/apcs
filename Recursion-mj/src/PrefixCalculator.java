import java.util.Scanner;
 
/*
*********************************************************************************
PREFIX Expression Notation.  

We are used to evaluating math expressions using 'infix' notation where
the operators are between the operands, for example

	 10 - 5

but this has a problem of ambiguity and completeness when more than
on operation is involved.   Thus 

	10 - 5 * 2

Can be interpreted as either 
	
	(10 - 5) * 2  == 10
	10 - (5 * 2) == 0
	
Notice we needed to introduce () to resolve the ambiguities.   THere is 
however a way of expressing operations that is simple, and unambiguous
and complete.   It is called prefix notation and basically you specify
the operator FIRST following by TWO  (where the operands might
be numbers but they also might be ANOTHER operators followed by its
operands (recursively).    For example

	- 10 * 5 2     means 10 - (5 * 2)
    * - 10 5 2	   means (10 - 5) * 2 

Walk through:  In the first example we see a - and then read two more
operators, the first is 10, but second is a * so we first have to
evaluate that, so we read to more operands (5 and 2) and do the operation
to get 10 and then finally we can do the original - 

Thus

	- 10 * 5 2  == - 10 (* 5 2)  ==  - 10 10  == 0 
	
	* - 10 5 2 == * (- 10 5) 2   ==  * 5 2 == 10 

You can also think of it as
 
	ANYWHERE in the input, if you see an operator followed by two
	numbers, then that can be evaluated, so do so, replacing it with
	its value.   Continue to do this until you are left with just
	a single number (if this does not happen it is a syntax error.  
	

1) REQUIRED:   Evaluate the following prefix expressions 

    A)  - 100 - 5 - 7 - 8 - 3 - 4 2 
    B)  * - + * 3 4 7 3 2
 
 
***********************************************************************
Exceptions

Throwing Exceptions
   You throw an exception by creating an instance of the type 
   and using the 'throw' expression.  For example to throw the
   java.lang.IllegalArgumentException you can do 
   
   throw new IllegalArgumentException("Argument foo = X is not an integer")
   
   Notice that it is important to put useful information into the exception
  
Catching exceptions
   You catch an exception by using the try { ... } catch(TYPE var) { ... }

	try {
		evaluate("%", 3, 5);
	} catch(IllegalArgumentException e)
	{
		System.out.println("Error: " + e.getMessage());
	}
	
**************************************************************************************
2) REQUIRED  Implement:  static int evaluate(String operator, int operand1, int operand2)
 
    This method takes an operator that must be a string + - * or /
    and two integers and does the operation
    If the operation is not one of the four it throws an IllegalArgumentException
    The exception has a good descriptive message, including the bad operator that was found.  
    The method comment should indicate when it throws (and what it throws).  
 
3) REQUIRED implement a test for the evaluate method.  There should be a separate
    test for each operator, and each such tests should do two or three calls.  
    
    You also should add a test that tests that the method throws when there is a 
    bad operator.  You can do this with the 'assertThrows method.   You pass to this
    method type of the method you expect (This is what the IllegalArgumentException.class does)
    and a Runnable (remember lambdas) that is the code that should be run to cause 
    the exception.   (notice it takes no arguments and simply calls 'evaluate'.  Here
    is the line.  
 
 	Assertions.assertThrows(IllegalArgumentException.class, () -> { evaluate("X", 0, 1); });
 	
 	Also add another test that tests that that evaluate throws an ArithmeticException if you 
 	make the method divide by zero.  
 	
 4) REQUIRED implement:  
 
  		public static public int evaluatePrefix(String prefixString)
 		private static int evaluatePrefix(Scanner scanner)
 		
	The first method simply creates a scanner can calls the second one.  
	
	The second one does the parsing.   It uses the 'hasNextInt()' if the value
	is simply an integer.  If it is that is the value it returns.  
	
	Otherwise it is an operator.  it should do a next() to fetch it 
	It then calls itself to evaluate each of its arguments and then calls
	evaluate to actually do the evaluation.   
	
	Write a main method that calls evaluatePrefix("* - + * 3 4 7 3 2");
	
 5) REQUIRED Change evaluatePrefix(Scanner scanner) that it logs messages
    to System.out using the logging technique described.   This means
    using {} in the log and using indenting (thus you need to add a 
    String indent argument to this method.   
    
    Show the output of that on both 

	  evaluatePrefix("* - + * 3 4 7 3 2")
	  evaluatePrefix("- 100 - 5 - 7 - 8 - 3 - 4 2")
	  evaluatePrefix("+ * 3 4 * 7 3")

	Notice how the logging makes it pretty clear what is going on.   
	
 7) EXTRA write tests that test evaluatePrefix(String prefixString).  
 
 	You should achieve FULL CODE COVERAGE (every line of code is executed).  
 	
 8) EXTRA  Write a main program that repeatedly prompts the user and uses Scanner
    its nextLine() method to get a line from the user.  If the line is
    empty it ends.  Otherwise it calls  evaluatePrefix(prefixString) and prints
    the result.  
    
    Run this program and give it a bad operator (e.g. % 3 4)  Notice that
    the program crashes 
    
9) EXTRA  Use a try-catch block to make it so that if the user gives 
    bad input, it catches the error, prints a useful message, and conintus
    prompting the user for more input.    
    
10) EXTRA implement 
 
 		public static String prefixToInfix(String prefixString)
 		private static String prefixToInfix(Scanner scanner)
 			
 	Which instead of evaluating to a number evaluates to 
 	parenthesized infix expression.    Thus
 	
 	prefixToInfix("+ * 3 4 * 7 3") == "((3 * 4) + (7 * 3))";
 	
11) EXTRA Add JUnit tests for prefixToInfix.  Achieve full code coverage.  

12) EXTRA PostFix notation

    Just as there is prefix operator notation there postfix operator notation.
    where the two operands PRECEED the operator.   Thus
    
    	10 3 + 4 *   means ((10 3 +) 4 *)  == (10 + 3) * 4  = 52
    	
    Implement 
    
     	public static String prefixToPostfix(String prefixString)
 		private static String prefixToPostfix(Scanner scanner)
 		
 	That reads a string in prefix notation and outputs in postfix notation.
 	Thus 
 	
 	prefixToInfix("+ * 3 4 * 7 3") == "3 4 * 7 3 * +"
 	
 13) EXTRA Postfix interrupter (HARD) Implement the following routines
  
   		public static public int evaluatePrefix(String postFixString)
 		private static int evaluatePrefix(Scanner scanner)
 	
 	Which evaluate a postfix string. 
 	
14) Write tests that test that if you convert prefix to postfix and evaulate
    the postfix you get the same answer as if you just evaluate the prefix
    expression 

 */
public class PrefixCalculator {

	public static void main(String args[])
	{
		System.out.println("Enter a prefix string to evaluate");
		Scanner scanner = new Scanner("+ 5 6");
		evaluatePrefix(scanner);
		System.out.println("Program Complete");
	}
	
	public static int evaluate(String operator, int operand1, int operand2)
	{
		return 0;  // TODO remove and implement
	}
	
	public static int evaluatePrefix(Scanner scanner)
	{
		return 0;  // TODO remove and implement
	}
	
	public static String prefixToInfix(Scanner scanner, String indent)
	{
		return "";  // TODO remove and implement
	}

}
