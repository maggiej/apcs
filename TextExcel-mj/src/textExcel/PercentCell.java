package textExcel;

public class PercentCell extends RealCell {
	//constructor
	public PercentCell(String text) {
		super(text);
	}
	
	//converts percent to decimal
	public double getDoubleValue() {
		String answer=getText().substring(0, getText().length()-1); //rids percent sign
		double answerDouble=Double.parseDouble(answer); //convert to double
		answerDouble*=.01; //moves decimal point
		return answerDouble;
	}
	
	//full cell text should give the decimal
	public String fullCellText() {
		return getDoubleValue()+"";
	}
	
	//abbreviated cell text should truncate the decimal
	public String abbreviatedCellText() {
		if(getText().indexOf(".")==-1) return getText();
		String number=getText().substring(0, getText().indexOf(".")); //takes everything before decimal
		String answer= ""+number+"%"; //adds percent sign
		answer+="          ";
		return answer.substring(0,10);
	}
}
