package textExcel;

//Update this file with your own code.

public class SpreadsheetLocation implements Location
{
	
	private String cellName;
	private int row;
	private String col;
	
	//accessor for cell name
	public String getCellName(){
		return cellName;
	}
	
    @Override //to get the row from the string
    public int getRow()
    {
        return row-1; 
    }

    @Override //to get the column from the string
    public int getCol()
    {
    	 if (col.equalsIgnoreCase("A")) return 0;
         if (col.equalsIgnoreCase("B")) return 1;
         if (col.equalsIgnoreCase("C")) return 2;
         if (col.equalsIgnoreCase("D")) return 3;
         if (col.equalsIgnoreCase("E")) return 4;
         if (col.equalsIgnoreCase("F")) return 5;
         if (col.equalsIgnoreCase("G")) return 6;
         if (col.equalsIgnoreCase("H")) return 7;
         if (col.equalsIgnoreCase("I")) return 8;
         if (col.equalsIgnoreCase("J")) return 9;
         if (col.equalsIgnoreCase("K")) return 10;
         return 11;
    }
    
    public SpreadsheetLocation(String cellName)
    {
        this.cellName=cellName;
        row=Integer.parseInt(cellName.substring(1));
        col=""+cellName.charAt(0);
    }

}
