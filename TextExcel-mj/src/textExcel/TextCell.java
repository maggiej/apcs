package textExcel;

public class TextCell implements Cell {
	
	private String text = "";
	
	public TextCell(String text){
		this.text=text;
	}
	
	//returns 10 characters
	public String abbreviatedCellText(){
		String abbrev= text+ "          ";
		return abbrev.substring(0,10);
	}
	
	//returns full text surrounded by quotes
	public String fullCellText(){
		return "\"" + text + "\"";
	}
}