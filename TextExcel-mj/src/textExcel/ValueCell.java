package textExcel;

public class ValueCell extends RealCell{
	
	//constructor
	public ValueCell(String text) {
		super(text);
	}
	
	public String abbreviatedCellText(){
		
		double d = Double.parseDouble(fullCellText()); // A double representing the #
		
		//format the # to the unit tests
		String s = String.format("%.10s", d);
		
		//truncate
		String abbrev=  String.format("%.10s", d) + "          ";
		return abbrev.substring(0,10);

	}
	
	
	//just returns the value bc no calculations involved
	public double getDoubleValue() {
		return Double.parseDouble(fullCellText());
	}
}
