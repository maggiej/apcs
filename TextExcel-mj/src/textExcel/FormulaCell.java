package textExcel;

public class FormulaCell extends RealCell{
	
	private Spreadsheet grid; //makes the spreadsheet a field so it can access the data
	
	//constructor
	public FormulaCell(String text, Spreadsheet grid) {
		super(text);
		this.grid=grid;
	}
	

	public double getDoubleValue() { //calculates the formula
	
		String expression = getText().substring(2, getText().length()-2); //rids the ()
		
		if(isValidNumber(expression)) return Double.parseDouble(expression); //if the formula is just a number, return the number

		String[] formulaParts = expression.split(" ");//break the formula along spaces
		
		if(formulaParts[0].equalsIgnoreCase("avg") || formulaParts[0].equalsIgnoreCase("sum")){ //if the formula starts with avg or sum
			SpreadsheetLocation start = new SpreadsheetLocation(formulaParts[1].substring(0, formulaParts[1].indexOf('-')));//top left cell
			SpreadsheetLocation end = new SpreadsheetLocation(formulaParts[1].substring(formulaParts[1].indexOf('-')+1));//bottom right cell
			double sum=0;
			for(int k=start.getRow(); k<= end.getRow(); k++){ //go through the chosen cells and add the values
				for(int j=start.getCol(); j<=end.getCol(); j++){
					sum+=(((RealCell)grid.getGrid()[k][j]).getDoubleValue());
				}
			}
			
			if(start.getCellName().equalsIgnoreCase(end.getCellName())){ //if there's only one chosen cell the sum is whatever's inside it
				sum=((RealCell)grid.getGrid()[start.getRow()][start.getCol()]).getDoubleValue();
			}
			
			if(formulaParts[0].equalsIgnoreCase("sum")) return sum; //for sum commmand
			if(formulaParts[0].equalsIgnoreCase("avg")) return sum/((end.getRow()-start.getRow()+1)*(end.getCol()-start.getCol()+1)); //for average command
		}
		
		//goes through the formula parts and converts references to numbers
		for(int i=0; i<formulaParts.length; i++) {
			if(Character.isLetter(formulaParts[i].charAt(0))){
				SpreadsheetLocation location = new SpreadsheetLocation(formulaParts[i]);
				formulaParts[i] = ((RealCell)grid.getGrid()[location.getRow()][location.getCol()]).getDoubleValue() + "";
			}
		}
		
		//rebuilds the expression string from the array after the references have been converted to values
		expression = "";
		for(int i =0; i<formulaParts.length; i++){
			expression += formulaParts[i]+" ";
		}
		
		//TODO: You know that this doesn't do anything, ye?   You should understand why....
		expression.trim();  
		
		//runs the calculator code
		return Double.parseDouble(produceAnswer(expression));
	}
	
	public String abbreviatedCellText() { //returns the evaluated expression
		String ret = getDoubleValue()+"          ";
		return ret.substring(0, 10);
	}

//below is all the copied calculator code from last semester, but with double.parsedouble instead of integer

	//TODO: Helper methods (like this and below) should be marked as private
	//checkpoint 4: evaluates complex +-*/ expressions left to right
	public static String produceAnswer(String input){
		
		//uses the method built in cp3 (evaluates one simple expression) to evaluate the input one simple expression at a time
		while(true){
			if(input.indexOf(" ")==-1){ //if there's only one token
				return evalSimpleExpression(input);
			}
		
			String op1 = getFirstWord(input); //stores first token
			input = skipFirstWord(input); //everything past first token
			
			if(input.indexOf(" ")==-1) {
				return evalSimpleExpression(op1 + " " + input); //if there's only two tokens 
			}
			
			String operator = getFirstWord(input); //stores operator
			input = skipFirstWord(input);//everything past second token
			
			if(input.indexOf(" ")==-1){ //if there's only three tokens
				return evalSimpleExpression(op1 + " " + operator + " " + input);
			}
			
			String op2 = getFirstWord(input);
			input = skipFirstWord(input); //past third token
			String oneSimpleExpression = op1 + " " + operator + " " + op2; //stores first set of three tokens
			
			if(evalSimpleExpression(oneSimpleExpression).charAt(0)=='<'){ //terminates if set of 3 evaluates to an error
				return evalSimpleExpression(oneSimpleExpression);
			}
			
			input = evalSimpleExpression(oneSimpleExpression) + " " + input; //input is updated after first operation is evaluated
		}
	}
	
	//same method as CP3. evaluates one simple expression with three tokens.
	public static String evalSimpleExpression(String input)
	{ 
		
		input=input.trim();
		
		if (input.indexOf(" ")==-1) { //if there's only one token return the token
			if(isValidNumber(input) || input.equals("")) return input;
			else return "<ERROR> Invalid value: " + input;
		}
		
		String op1 = getFirstWord(input);
		
		if (!isValidNumber(op1)){ //if first operator is not a valid number
			return "<ERROR> Invalid value: " + op1;
		}
		
		input = skipFirstWord(input);
		
		if(input.indexOf(" ")==-1){ //if there's no token after the operator
			if( !input.equals("+") && !input.equals("-") && !input.equals("*") && !input.equals("/")){ //invalid operator error takes precedence
	    		return "<ERROR> Invalid operator encountered: " + input;
	    	}else{
	    		return "<ERROR> Invalid expression format.";
	    	}
		}
		
		String operator = getFirstWord(input);
		
		if( !operator.equals("+") && !operator.equals("-") && !operator.equals("*") && !operator.equals("/")){ //if operator is invalid
			return "<ERROR> Invalid operator encountered: " + operator;
		}
		
		input = skipFirstWord(input);
		
		if(input.indexOf(" ")!=-1){ //if there are more tokens than 3, making it a complex expression
			return "<ERROR> Invalid expression format.";
		}
		
		String op2 = input;
		
		if (op2.equals("0")&& operator.equals("/")) { //checks for divide by zero error
			return "<ERROR> Cannot divide by zero.";
		} else if (!isValidNumber(op2)){ //if second operator is not a valid number
			return "<ERROR> Invalid value: " + op2;
		}
		
		//evaluates the expression if valid.
		if(operator.equals("+")) {
			return Double.parseDouble(op1) + Double.parseDouble(op2) + "";
		} else if (operator.equals("-")) {
			return Double.parseDouble(op1) - Double.parseDouble(op2) + ""; 
		} else if (operator.equals("*")) {
			return Double.parseDouble(op1) * Double.parseDouble(op2) + "";
		} else {
			return Double.parseDouble(op1) / Double.parseDouble(op2) + "";
		}
		
	}
	
	//returns the first token in a string input
	public static String getFirstWord(String input) {
		input=input.trim();
		return input.substring(0, input.indexOf(" ")); //from start to the first space
	}
	
	//returns string input - the first token
	public static String skipFirstWord(String input) { //from just past first space to end
		input=input.trim();
		return input.substring(input.indexOf(" ")+1);
		
	}
	
	public static boolean isValidNumber(String strVal)
	{
	    try
	    {
	    	Double.parseDouble(strVal);             
	    }
	    catch (NumberFormatException e) 
	    {
	        return false;
	    }
	    return true;        
	}

}