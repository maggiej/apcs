package textExcel;
import java.io.*;
import java.util.*;

// Update this file with your own code.

public class Spreadsheet implements Grid
{
	//constants for grid dimensions
	private static final int MAX_HUMAN_ROW = 20;
	private static final int MAX_HUMAN_COLUMN = 12;
	
	//field: a 2D array
	private Cell[][] grid;
	
	//constructor: creates a 2d array w specified dimensions
	public Spreadsheet(){
		grid=new Cell[MAX_HUMAN_ROW][MAX_HUMAN_COLUMN];
		
		//fills array w emptycells
		fillWithEmptyCells(grid);
		
	}
	
	//accessor
	public Cell[][] getGrid(){
		return grid;
	}
	
	//fills the spreadsheet with empty cells
	public static void fillWithEmptyCells(Cell[][] grid){
		for(int row=0; row<grid.length; row++){
			for(int col=0; col<grid[row].length; col++){
				grid[row][col]=new EmptyCell();
			}
		}
	}

	@Override
	//processes the command and returns the resulting string
	public String processCommand(String command)
	{
		command = command.trim();//rids extra spaces
		if (command.equals("")) {//for empty commands
			return "";
		}
		
		String[] tokens = command.split(" ", 3);//splits the command into three parts
		
		if(tokens.length==1){//if command is only one part it must be clear or cell name
			
			if(tokens[0].equalsIgnoreCase("clear")){//if command is to clear
				fillWithEmptyCells(grid);
				return getGridText();//print the grid
			}
			
			else{//if the command is to inspect a cell
				SpreadsheetLocation location=new SpreadsheetLocation(command);//determine the location
				
				return grid[location.getRow()][location.getCol()].fullCellText();//print the cell at that location
			}
			
		}
		
		if(tokens.length==2){//if 2 tokens long, command must be to clear a cell or to open/save
			if(tokens[0].equalsIgnoreCase("save")){ //save command
				try {
					PrintWriter writer = new PrintWriter(tokens[1]); //name of file=second token
					for(int i = 0; i<MAX_HUMAN_ROW; i++) {
						for(int j = 0; j<MAX_HUMAN_COLUMN; j++) { //loops thru every cell
							if(!(grid[i][j] instanceof EmptyCell)) { //if it aint empty
								char rowLetter = (char) ('A' + j); //row letter with char arithmetic
								String cellName = Character.toString(rowLetter) + (i + 1); //comes up with cell name
								writer.println(cellName + "," + getCellType(grid[i][j]) + "," + grid[i][j].fullCellText()); //stores the info in the file
							}
						}
					}
					writer.close();
					return getGridText();
				} catch (FileNotFoundException e) {
					System.out.println(e.getMessage());
				}
				
			//open command	
			} else if (tokens[0].equalsIgnoreCase("open")) {
				try {
					fillWithEmptyCells(grid); //clear first
					Scanner reader = new Scanner(new File(tokens[1])); //second token = file name
					while(reader.hasNext()) {
						String s = reader.nextLine(); //for each line
						String[] commaSplit = s.split(",", 3); //split into 3 along commas
						SpreadsheetLocation location=new SpreadsheetLocation(commaSplit[0]);
						//creates cells according to type
						if(commaSplit[1].equalsIgnoreCase("FormulaCell")) {
							grid[location.getRow()][location.getCol()]=new FormulaCell(commaSplit[2], this);
						} else if(commaSplit[1].equalsIgnoreCase("PercentCell")) {
							double percent = Double.parseDouble(commaSplit[2])*100; //converts into percent 
							grid[location.getRow()][location.getCol()]=new PercentCell(percent+"");
						} else if(commaSplit[1].equalsIgnoreCase("TextCell")) {
							grid[location.getRow()][location.getCol()]=new TextCell(commaSplit[2].substring(1, commaSplit[2].length()-1)); //gets rid of quotes
						} else {
							grid[location.getRow()][location.getCol()]=new ValueCell(commaSplit[2]);
						}
					}
					reader.close();
					return getGridText();
				} catch(FileNotFoundException e) {
					System.out.println(e.getMessage());
				}
				
			} else{
				SpreadsheetLocation location=new SpreadsheetLocation(tokens[1]);//locates the cell
				grid[location.getRow()][location.getCol()]=new EmptyCell();//makes it an empty cell
				return getGridText();
			}
		}
		
		//command is three tokens or more and thus is an assignment
		SpreadsheetLocation location=new SpreadsheetLocation(tokens[0]);//first token is the cell location
		//creates textcell if starts w quotations
		if(tokens[2].charAt(0)=='\"') {
			grid[location.getRow()][location.getCol()]=new TextCell(tokens[2].substring(1,tokens[2].length()-1));//the cell at that location becomes a textcell with the input after =	(which is the substring)	
		} else if (tokens[2].charAt(0)=='(') { //creates formulacell if starts w parentheses
			grid[location.getRow()][location.getCol()]=new FormulaCell(tokens[2], this);	
		} else if (tokens[2].charAt(tokens[2].length()-1)=='%') {//creates percent cell if it ends w %
			grid[location.getRow()][location.getCol()]=new PercentCell(tokens[2]);
		} else { //creates value cell
			grid[location.getRow()][location.getCol()]=new ValueCell(tokens[2]);
		}
		return getGridText();
	}
	
	// Figure out what type a cell is and return the string representing the type
	public String getCellType(Cell input) {
		if (input instanceof FormulaCell) {
			return "FormulaCell";
		} else if (input instanceof PercentCell) {
			return "PercentCell";
		} else if (input instanceof TextCell) {
			return "TextCell";
		} else if (input instanceof EmptyCell){
			return "EmptyCell";
		} else {
			return "ValueCell";
		}	
	}

	@Override
	//returns number of rows
	public int getRows()
	{
		return grid.length;
	}

	@Override
	//returns number of columns
	public int getCols()
	{
		return grid[0].length;
	}

	@Override
	//returns a cell at the specified location
	public Cell getCell(Location loc)
	{
		return grid[loc.getRow()][loc.getCol()];
	}

	@Override
	//prints the whole grid
	public String getGridText()
	{
		String fullGrid="";//string to build off of
		
		//first row with headers
		String[] abc = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L"};
		for (int i=0; i<=MAX_HUMAN_COLUMN; i++){
			if (i==0){
				fullGrid= "   |";
			} else{
				fullGrid += abc[i-1] + "         |";
			}
		}
		
		//numbered rows and spaces
		for (int i=0; i<MAX_HUMAN_ROW; i++){
			
			fullGrid += "\n" + (i+1);
			if(i<9){
				fullGrid += "  |";
			} else {
				fullGrid += " |";
			}
			
			//for every cell put in the 10 character abbreviation
			for (int j=0; j<MAX_HUMAN_COLUMN; j++){
					fullGrid+= grid[i][j].abbreviatedCellText() + "|";
				}
		}
		fullGrid+="\n";
		return fullGrid;
	}

}

