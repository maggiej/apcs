package textExcel;

public abstract class RealCell implements Cell{
	
	private String text;
	
	public RealCell(String text) {
		this.text=text;
	}
	
	public String getText(){
		return text;
	}
	
	//returns 10 characters. truncates not rounds.
		public String abbreviatedCellText(){
			String abbrev= text+ "          ";
			return abbrev.substring(0,10);
		}

	//returns full text
	public String fullCellText() {
		return text;
	}
	
	//to be overriden 
	public abstract double getDoubleValue();
}
