/*
 * Grading (TextExcelB):  62/64
 * Deductions:  (See below)
 * 
 * Look for TODO comments in your code (or search on "tasks" in the quick access window) to quickly view all of them
 * For this project - Please correct (or address) and TODO items for the next part.
 * 
 * General comments:
 *  Lateness of Submission: OK
 *  Undone TODOs: OK
 *  Coding/Style/Naming (10): -2 (see my TODOs in FormulaCell)
 *  Arith Formulas & AVG/SUM (10): OK
 *  Final Tests Passing (35): OK  
 *  Three Branches created (9): OK
 *  Extra Credit (command error) (3): 0
 *  Extra Credit (circular reference errors) (3): 0
 *  Extra Credit (evaluation error) (3): 0  
 *  Extra Credit (order of operations) (3): 0
 *      
 *  Other comments:   Why no extra credit, Maggie?  I was hoping some of those problems would interest you.
 *  	Otherwise, really nice job - Good commenting and you have somethign that really works!
 *      
 */


//textexcel cp3!!!

//maggie jiang
//2.28.19
//period 1 apcs
//this program runs the command loop and prompts the user

package textExcel;

import java.io.FileNotFoundException;
import java.util.Scanner;

public class TextExcel
{

	public static void main(String[] args)
	{
	    Spreadsheet excel=new Spreadsheet();//creates the spreadsheet
	   
	    Scanner console = new Scanner(System.in);
	    
	    System.out.println("please enter an input or \"quit\" to quit");
	    
	    String input=console.nextLine();//first command
	    
	    while(!(input.equalsIgnoreCase("quit"))){//keeps taking commands until quit
	    	System.out.println(excel.processCommand(input));
	    	System.out.println("please enter an input or \"quit\" to quit");
	    	input=console.nextLine(); 
	    }
	    
	    System.out.println("goodbye!");
	    
	}
	
}
