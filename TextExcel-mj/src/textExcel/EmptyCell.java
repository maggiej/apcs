package textExcel;

public class EmptyCell implements Cell {
	
	//returns 10 characters
	public String abbreviatedCellText(){
		return "          ";
	}
	 
	//returns empty string for this checkpoint
	public String fullCellText(){
		return "";
	}
}
