/*
 * Grading:  23.5/25
 * Deductions:  (See below)
 * 
 * Look for TODO comments in your code (or search on "tasks" in the quick access window) to quickly view all of them
 * Please understand what you did wrong and avoid these problems in the future!
 * 
 * General comments:
 *  1) Pseudocode: Ok, you realize that you basically used your code, yes? ;-)
 *  2) Single game playing: Good!  (I like how you validate input)
 *  3) Multiple games: -0.5 for not drawing line in between round.
 *  4) Statistics: Good!
 *  5) Comments/Style: -1 (global variables), otherwise, good job!
 *  
 *  General:   Nicely done Maggie ;-)
 */

//Maggie Jiang
//11.17.18
//Period 1 APCS
/*In this program, the user plays rock paper scissors with the computer
  for as many rounds as the user desires. 
*/

import java.util.*;

public class RockPaperScissors {
	
	//TODO: Ick - This is a global variable - Don't use this in programs as it makes it unmaintainable over the long run.
	static Scanner console = new Scanner(System.in);
	
	public static void main(String[] args){
		
		//TODO: Note:  One statement methods aren't really that helpful - I would have moved the playgame logic in here (and then factored out any extra code in that method elsewhere)
		playGame();
	}
	
	//static method to repeatedly play rounds until the user wants to stop.
	public static void playGame(){
		int round = 1;
		String continueGame="dummy value"; //not the sentinel, so at least one round will be played
		
		//plays rounds until the sentinel
		while (!(continueGame.equalsIgnoreCase("no"))){ 
			playOneRound(round);
			round++; //ensures the round number goes up each round
			System.out.println();
			
			//asks user if they want to play another round
			System.out.println("Would you like to continue playing? Enter yes or no");
			continueGame=console.next();
			
			//TODO: This type of thing below I would say would be a good choice for it's own helper method.
			//ensures that user types a valid response
			while (!(continueGame.equalsIgnoreCase("yes")||continueGame.equalsIgnoreCase("no"))){
				System.out.println("Invalid response. Please enter yes or no.");
				continueGame = console.next();
			}
			System.out.println();
		}
		
		System.out.println("Thanks for playing!");
	}
	
	//static method to play one round
	public static void playOneRound(int round){
		System.out.println("Beginning round " + round);
		int moves = 0;
		
		//strings start out with same values so following loop will run 
		String computerChoice = "dummy value";
		String userChoice = "dummy value";
		
		//prompts user and computer until not a tie
		while(computerChoice.equalsIgnoreCase(userChoice)){
			userChoice = promptUser(); //stores user response
			computerChoice = computerPick(); //stores computer's pick
			System.out.println("Computer picked " + computerChoice);
			moves++; //counts the number of moves
			
			//prints results if user wins
			if (userChoice.equalsIgnoreCase("rock")&&computerChoice.equalsIgnoreCase("scissors")
				||userChoice.equalsIgnoreCase("paper")&&computerChoice.equalsIgnoreCase("rock")
				||userChoice.equalsIgnoreCase("scissors")&&computerChoice.equalsIgnoreCase("paper")){
				
				System.out.println("Round " + round + " \tYou win!\t # of moves is " + moves);
			
			//prints results if computer wins
			} else if ((userChoice.equalsIgnoreCase("rock")&&computerChoice.equalsIgnoreCase("paper")
					||userChoice.equalsIgnoreCase("paper")&&computerChoice.equalsIgnoreCase("scissors")
					||userChoice.equalsIgnoreCase("scissors")&&computerChoice.equalsIgnoreCase("rock"))){
				
				System.out.println("Round " + round + " \tComputer wins\t # of moves is " + moves);
			
			//notifies user that it's a tie. will loop again
			} else {
				System.out.println("No winner. It's a tie. Continuing round.");
				System.out.println();
			}
		}
		
	}
	
	//static method that prompts user to pick rock paper or scissors. returns response.
	public static String promptUser() {
		System.out.println("Enter rock, paper, or scissors.");
		String userChoice = console.next();
		
		//ensures user response is valid
		while (!(userChoice.equalsIgnoreCase("rock")
		|| userChoice.equalsIgnoreCase("paper")
		|| userChoice.equalsIgnoreCase("scissors"))) {
			
			System.out.println("Invalid response. Please enter rock, paper, or scissors.");
			userChoice = console.next();
		}
		
		return userChoice;
		
	}
	
	//static method to have computer pick rock paper or scissors. returns result.
	public static String computerPick() {
		
		//picks a random integer 0, 1, or 2
		int choice = (int)(Math.random()*3);
		
		//assigns rock, paper, scissors to those integer values.
		if (choice==0){
			return "rock";
		} else if (choice==1){
			return "paper";
		} else {
			return "scissors";
		}
		
	}
		

}
