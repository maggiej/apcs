//Maggie Jiang
//2.23.19
//Period 1 APCS
//This class specifies general fields and methods for Vehicle objects

public class Vehicle {
	
	//field number of seats
	private int numOfSeats;
	
	//constructor with number of seats
	public Vehicle(int seats) {
		numOfSeats=seats;
	}
	
	//returns number of seats
	public int getSeats() {
		return numOfSeats;
	}
	
}
