//Maggie Jiang
//2.23.19
//Period 1 APCS
//This class specifies state and behavior for Bird objects

public class Bird extends Animal implements FlyingObject{
	
	//birds are animals, constructed the same way
	public Bird(int age) {
		super(age);
	}
	
	//implementation of FlyingObject fly method
	public void fly() {
		System.out.println("Bird flying");
	}
	
	//prints String representation of Bird
	public String toString() {
		//TODO: Note calling super. here is unnecessary b/c you already have access to this since it's inherited...
		return "Bird is " + super.getAge() + " years old";
	}
}
