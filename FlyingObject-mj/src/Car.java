//Maggie Jiang
//2.23.19
//Period 1 APCS
//This class specifies state and behavior for Car objects

public class Car extends Vehicle{
	
	//car is a vehicle, constructed the same way
	public Car(int seats) {
		super(seats);
	}
	
	//returns string representation of Car
	public String toString() {
		return "Car seats " + super.getSeats();
	}

}
