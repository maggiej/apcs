//Maggie Jiang
//2.23.19
//Period 1 APCS
//This class specifies general state and behavior for Animal objects

public class Animal {
	
	//field: age of animal
	private int age;
	
	//constructor: constructed with age
	public Animal(int age) {
		this.age=age;
	}
	
	//returns age
	public int getAge() {
		return age;
	}
}
