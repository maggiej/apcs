//Maggie Jiang
//2.23.19
//Period 1 APCS
//This class specifies state and behavior for Plane objects

public class Plane extends Vehicle implements FlyingObject {
	
	//planes are vehicles, constructed the same way
	public Plane(int seats) {
		super(seats);
	}
	
	//implementation of FlyingObject's fly method
	public void fly() {
		System.out.println("Plane flying");
	}
	
	//returns string representation of Plane
	public String toString() {
		return "Plane seats " + super.getSeats();
	}

}
