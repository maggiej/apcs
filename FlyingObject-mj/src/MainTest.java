/*
 * Grading:  7/7
 * Deductions:
 * 	Project Name: Ok
 * 	Comments: Ok
 * 	Authorship: Ok
 * 	Style:
 * 
 * Implementation:
 * 	Animal: Ok
 * 	Bird: Ok
 * 	Car: Ok
 * 	Elephant: Ok
 * 	FlyingObject: Ok
 * 	Plane: Ok
 * 	Vehicle: Ok
 * 	 
 
 * Look for TODO comments in your code (or search on "tasks" in the quick access window) to quickly view all of them
 * Please understand what you did wrong and avoid these problems in the future!
 * 
 * General comments: See comment in bird, otherwise good.  
 * 
 */


public class MainTest
{
	/*
	 * Uncomment the main() code when you have completed all your classes. 
	 * The expected output should be:
	 * 
	 * 		The animals are:
	 *		Bird is 10 years old
	 * 		Elephant is 20 years old.
	 *
	 *		The vehicles are:
	 * 		Plane seats 200
	 *		Car seats 4
	 *
	 *		The flying objects are:
	 * 		Bird flying
	 *		Plane flying 
	 */
	
	
    public static void main(String[] args)
    {  
    	
        Bird bird = new Bird(10);                
        Elephant elephant = new Elephant(20);
        Plane plane = new Plane(200);
        Car car = new Car(4);
        
        System.out.println("The animals are:");
        displayAnimal(bird);
        displayAnimal(elephant);
        System.out.println();
         
        System.out.println("The vehicles are:");
        displayVehicle(plane);
        displayVehicle(car);
        System.out.println();
        
        System.out.println("The flying objects are:");
        takeFlight(bird);
        takeFlight(plane);
    }

	// polymorphism through interface
    public static void takeFlight(FlyingObject obj)
    {
        obj.fly();
    }

	// polymorphism through inheritance
    public static void displayAnimal(Animal animal)
    {
        System.out.println(animal);
    }
    
    // polymorphism through inheritance
    public static void displayVehicle(Vehicle vehicle)
    {
        System.out.println(vehicle);
    }
    
}
