//Maggie Jiang
//2.23.19
//Period 1 APCS
//This interface is implemented by Plane and Bird classes

//all FlyingObjects have a method fly
public interface FlyingObject {
	public void fly();
}
