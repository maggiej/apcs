//Maggie Jiang
//2.23.19
//Period 1 APCS
//This class specifies state and behavior for Elephant objects

public class Elephant extends Animal {
	
	//Elephants are animals, constructed the same way
	public Elephant(int age){
		super(age);
	}
	
	//returns String representation of Elephant
	public String toString(){
		return "Elephant is " + super.getAge() + " years old.";
	}
}
