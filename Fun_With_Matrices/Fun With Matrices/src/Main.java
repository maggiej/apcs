/*
 * Grading:  23.5/25 
 * Deductions: 
 * 	-1.5 (wrong magic solution)
 * 
 * Look for TODO comments in your code (or search on "tasks" in the quick access window) to quickly view all of them
 * Please understand what you did wrong and avoid these problems in the future!
 * 
 * General comments:
 * 	Overall, a solid job - Your commenting and code are (mostly) fun to read (why didn't you take a stab at the bonus ones!!?)
 * 
 */

//Maggie Jiang
//12.15.18
//Period 1 APCS
//This program contains several methods involving 2D arrays

public class Main
{
    public static final int[][] MATRIX_1 = { {3, 3, 3}, {1, 1, 1}, {2, 2, 2}};
    public static final int[][] MATRIX_2 = { {0, 0, 3}, {0, 2, 2}, {1, 1, 1}};
    
    // Do you believe in magic?
    public static final int[][] MAGIC_MATRIX_1 = {{2, 7, 6}, {9, 5, 1}, {4, 3, 8}}; // yes
    public static final int[][] MAGIC_MATRIX_2 = {{1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}}; // yes
    public static final int[][] MAGIC_MATRIX_3 = {{2, 7, 12, 13}, {16, 9, 6, 3}, {5, 4, 15, 10}, {11, 14, 1, 8}}; // yes
    public static final int[][] MAGIC_MATRIX_4 = {{2, 7, 6}, {9, 5, 1}, {4, 1, 8}}; // no
    public static final int[][] MAGIC_MATRIX_5 = {{2, 7, 6}, {9, 5, 1}, {5, 3, 8}}; // no
    public static final int[][] MAGIC_MATRIX_6 = {{1, 2, 3, 4}, {2, 3, 4, 1}, {3, 4, 1, 2}, {4, 1, 2, 3}}; // no    

    public static void main(String[] args)
    {
    	
 	
    	//tests for exercise 1
    	printMatrix(MATRIX_2, false);
    	printMatrix(MATRIX_2, true);
    	
    	//tests for exercise 2
    	printMatrix(MATRIX_1, true);
    	shiftUp(MATRIX_1);
    	printMatrix(MATRIX_1, true);
    	
    	printMatrix(MATRIX_2, true);
    	shiftUp(MATRIX_2);
    	printMatrix(MATRIX_2, true);
    	
    	//tests for exercise 3
    	printMatrix(numberPyramid(7), false);
    	printMatrix(numberPyramid(1), false);
    	printMatrix(numberPyramid(4), false);
    	
    	//tests for exercise 4
    	System.out.println(isMagicSquare(MAGIC_MATRIX_1)); //true
    	System.out.println(isMagicSquare(MAGIC_MATRIX_2)); //true
    	System.out.println(isMagicSquare(MAGIC_MATRIX_3)); //true
    	System.out.println(isMagicSquare(MAGIC_MATRIX_4)); //false
    	System.out.println(isMagicSquare(MAGIC_MATRIX_5)); //false
    	System.out.println(isMagicSquare(MAGIC_MATRIX_6)); //false
    	
    	//tests for exercise 5
    	System.out.println(calculateSum(MAGIC_MATRIX_3, 1, 1, 2, 3)); //47
    	System.out.println(calculateSum(MAGIC_MATRIX_1, 0, 0, 2, 1)); //30
    	
    }
    
    //exercise 1. this method prints a nicely formatted 1d array, aka one row of a 2d array
    public static void printMatrixRow(int[] row, boolean printZeroes) {
    	for(int elem : row) { //for each element in the row
    		if(printZeroes || elem != 0){ //print the element if it's not a zero or if it's a zero but we're choosing to print zeroes
    			System.out.printf("%5d", elem);
    		} else { //if the element is a zero and we're not printing zeroes, print a blank space
    			System.out.printf("%5s", " ");	
    		}		
    	}
    	System.out.println();
    }
    
    //exercise 1 cont. this method prints a nicely formatted 2d array row by row w/ the columns aligned
    public static void printMatrix(int[][] matrix, boolean printZeroes) {
    	for(int[] row : matrix) { //for each row, print the elements in the row
    		printMatrixRow(row, printZeroes);
    	}
    	System.out.println();
    }
    
    //exercise 2. this method shifts up the rows in a 2d array.
    public static void shiftUp(int[][] matrix){
    	int[] temp = matrix[0]; //stores the first row in a temporary 1d array
    	for(int row = 0; row<matrix.length-1; row++){ //for each row except the last one, assign it the values of the next row
    		matrix[row]=matrix[row+1];
    	}
    	matrix[matrix.length-1]=temp; //for the last row, assign it the values of the first row by assigning it the values of temp
    }
    
    //exercise 3. returns a pyramid of numbers with n rows as a 2d array
    public static int[][] numberPyramid(int n){
    	int[][] matrix = new int[n][n]; //creates the matrix
    	for(int row = 0; row<matrix.length; row++){
    		for(int column = matrix.length-1; column>=matrix.length-1-row; column--){ //starts from last column
    			matrix[row][column]=row+1;
    		}
    	}
    	return matrix;
    }
    
    //exercise 4. checks if it's a magic square
    public static boolean isMagicSquare(int[][] matrix){
    	
    	//checks if same # of rows and columns
    	if(matrix.length!=matrix[0].length){
    		return false;
    	}
    	
    	//checks if all the rows add up to the same thing
    	for(int row = 0; row<matrix.length-1; row++){
    		int sum1 = 0;
    		
    		//TODO: This logic seems like overkill (nested for loops) can be made much simpiler...
    		for(int column = 0; column< matrix[row].length; column++){
    			sum1+=matrix[row][column];
    		}
    		
    		int sum2=0;
    		for(int column = 0; column<matrix[row].length; column++){
    			sum2+=matrix[row+1][column];
    		}
    	
    		if (sum1!=sum2) return false;
    	}
    	
    	// Note:  Not only do the rows and columns and diags need to add up, but they all need to add up to the same #!
    	// Your logic is not quite right - You should just compare everything against the same #
    	
    	//checks if all the columns add up to the same thing
    	for(int column = 0; column<matrix[0].length-1; column++){
    		int sum1=0;
    		for(int row = 0; row<matrix.length; row++){
    			sum1+=matrix[row][column];
    		}
    		
    		int sum2=0;
    		for(int row = 0; row<matrix.length; row++){
    			sum2+=matrix[row][column+1];
    		}
    		
    		if (sum1!=sum2) return false;
    	}
    	
    	//checks if the two diagonal sums are equivalent
    	int diagonal = 0;
    	for(int row = 0; row<matrix.length; row++){
    		diagonal += matrix[row][row];
    	}
    	
    	int diagonal2 = 0;
    	for(int row = 0; row<matrix.length; row++){
    		diagonal2 += matrix[row][matrix[row].length-1-row];
    	}
    	if (diagonal!=diagonal2) return false;
    	
    	return true;
    }
    
    //exercise 5. returns the sum of a submatrix within a matrix.
    public static int calculateSum(int[][] matrix, int topLeftRow, int topLeftColumn, int bottomRightRow, int bottomRightColumn) {
    	int sum = 0;
    	for(int row = topLeftRow; row<= bottomRightRow; row++) { //accounts for the specified rows
    		for(int column = topLeftColumn; column<=bottomRightColumn; column++) { //accounts for the specified columns in each row
    			sum+=matrix[row][column];
    		}
    	}
    	return sum;
    }

}
