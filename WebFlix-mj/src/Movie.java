/*
 * Grading:  15/15
 * Deductions:  (See below)
 * 
 * Look for TODO comments in your code (or search on "tasks" in the quick access window) to quickly view all of them
 * Please understand what you did wrong and avoid these problems in the future!
 * 
 * General comments:
 *  Coding/Style/Naming: 
 *  Movie: Ok
 *  Client: Ok
 *  Manager: Ok
 *   
 *  General: Well done Maggie!
 */

//Maggie Jiang
//1.17.19
//Period 1 APCS
//This class is a blueprint for movie objects

//you may assume parameters passed are valid
public class Movie {
	
	//fields
	private String title;
	private String director;
	private int yearReleased;
	private boolean watched;
	
	//constructs a new Movie object
	public Movie(String title, String director, int yearReleased) {
		this.title=title;
		this.director=director;
		this.yearReleased=yearReleased;
		watched=false;
	}
	
	//returns the title of the Movie
	public String getTitle() {
		return title;
	}
	
	//returns the director of the Movie
	public String getDirector() {
		return director;
	}
	
	//returns the year released
	public int getYearReleased() {
		return yearReleased;
	}
	
	//returns whether the Movie has been watched
	public boolean isWatched() {
		return watched;
	}
	
	//marks the Movie as watched
	public void watch() {
		watched=true;
	}
	
	//checks if one Movie is the same as another
	public boolean isSame(Movie other) {
		return title.equals(other.title) && director.equals(other.director) && yearReleased==other.yearReleased;
	}
	
	//returns a String representation of the Movie
	public String toString() {
		return title + ", " + director + ", " + yearReleased;
	}
}
