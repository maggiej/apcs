import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

/**
 * Webflix project tests
 * 
 * @author jeverard (mostly)
 */
class Tests {

	@Test
	public void testMovie() {
		
		Movie m = new Movie("Titanic", "James Cameron", 1999);
		Movie m2 = new Movie("Titanic", "James Cameron", 1999);
		assertEquals(false, m.isWatched());
		assertEquals("Titanic",m.getTitle());
		assertEquals("James Cameron",m.getDirector());
		assertEquals(1999,m.getYearReleased());
		
		assertEquals(true, m2.isSame(m2));
		assertEquals(false, m2.isSame(new Movie("Titani", "James Cameron", 1999) ));
		assertEquals(false, m2.isSame(new Movie("Titanic", "James Doogie", 1999) ));
		assertEquals(false, m2.isSame(new Movie("Titanic", "James Cameron", 1998) ));
		
		assertEquals("Titanic, James Cameron, 1999",m.toString());				
		
	}
	
	@Test
	public void testMovieManager1() {
		
		WebflixManager manager1 = new WebflixManager();
		assertEquals(0,manager1.size());
		assertEquals(null,  manager1.nextUnwatchedMovie());
		manager1.watchMovie();
		assertEquals(0,manager1.size());
		assertEquals(null,  manager1.nextUnwatchedMovie());
		
		assertEquals(true, manager1.addMovie("The Da Vinci Code", "Ron Howard", 2009));
		
		assertEquals(true,manager1.addMovie("The Avengers", "The Russo Brothers", 2012));
		assertEquals(true,manager1.addMovie("The Secret Life of Walter Mitty", "Ben Stiller", 2013)); //true
		assertEquals(true,manager1.addMovie("Moana", "Ron Clements, John Musker", 2016)); //true
		assertEquals(true,manager1.addMovie("Fantastic Beasts and Where to Find Them", "David Yates", 2016)); //true
		assertEquals(false,manager1.addMovie("The Da Vinci Code", "Ron Howard", 2009)); //false, same movie
		assertEquals(true,manager1.addMovie("Now You See Me", "Louis Leterrier", 2013)); //true
		assertEquals(true,manager1.addMovie("El Tiempo Entre Costuras", "I�aki Mercero", 2013)); //true
		assertEquals(true,manager1.addMovie("National Treasure", "Jon Turteltaub", 2004)); //true
		assertEquals(true,manager1.addMovie("The Russians are Coming, the Russians are Coming", "Norman Jewison", 1966)); //true
		assertEquals(true,manager1.addMovie("La La Land", "Damien Chazelle", 2016));
		assertEquals(10,manager1.size());
		assertEquals(false,manager1.addMovie("Back to the Future", "Robert Zemeckis", 1985)); // 10 already added
		assertEquals(10,manager1.size());

	}
	
	@Test
	public void testMovieManager2() {
		
		Movie one = new Movie("The Da Vinci Code", "Ron Howard", 2009); 
		Movie two = new Movie("The Avengers", "The Russo Brothers", 2012); 
		Movie three = new Movie("The Secret Life of Walter Mitty", "Ben Stiller", 2013);
		Movie four = new Movie("Moana", "Ron Clements, John Musker", 2016);
		Movie five = new Movie("Fantastic Beasts and Where to Find Them", "David Yates", 2016);
		Movie six = new Movie("Now You See Me", "Louis Leterrier", 2013);
		Movie seven = new Movie("El Tiempo Entre Costuras", "I�aki Mercero", 2013);
		Movie eight = new Movie("National Treasure", "Jon Turteltaub", 2004);
		Movie nine = new Movie("The Russians are Coming, the Russians are Coming", "Norman Jewison", 1966);
		Movie ten = new Movie("La La Land", "Damien Chazelle", 2016);
		Movie eleven = new Movie("Back to the Future", "Robert Zemeckis", 1985);
		
		Movie[] movieList1 = {one, two, three, four, five, six, seven, eight, nine, ten, eleven};
				
		//General case
		WebflixManager manager2 = new WebflixManager(movieList1);
		assertEquals(10,manager2.size());
		
		String s = "The Da Vinci Code\n" + 
				"The Avengers\n" + 
				"The Secret Life of Walter Mitty\n" + 
				"Moana\n" + 
				"Fantastic Beasts and Where to Find Them\n" + 
				"Now You See Me\n" + 
				"El Tiempo Entre Costuras\n" + 
				"National Treasure\n" + 
				"The Russians are Coming, the Russians are Coming\n" + 
				"La La Land";
		
		assertEquals(s, manager2.toString());
		
		for(int i = 0; i < 4; i++) //watches first four movies
			manager2.watchMovie();

		assertEquals("Fantastic Beasts and Where to Find Them",manager2.nextUnwatchedMovie().getTitle());
		
		for(int i = 4; i < movieList1.length; i++) //watches the rest of the movies
			manager2.watchMovie();		
		
		manager2.watchMovie();		
		assertEquals(null, manager2.nextUnwatchedMovie());

	}	
	
	@Test
	public void testMovieManager3() {
		
		Movie one = new Movie("The Da Vinci Code", "Ron Howard", 2009); 
		Movie two = new Movie("The Avengers", "The Russo Brothers", 2012); 
		Movie three = new Movie("The Secret Life of Walter Mitty", "Ben Stiller", 2013);
		Movie four = new Movie("Moana", "Ron Clements, John Musker", 2016);
		Movie five = new Movie("Fantastic Beasts and Where to Find Them", "David Yates", 2016);
		Movie six = new Movie("Now You See Me", "Louis Leterrier", 2013);
		
		Movie[] movieList2 = {one, two, three, four, five, six};
				
		//General case
		WebflixManager manager2 = new WebflixManager(movieList2);				
		assertEquals(6,manager2.size());
								
		String s = "The Da Vinci Code\n" + 
				"The Avengers\n" + 
				"The Secret Life of Walter Mitty\n" + 
				"Moana\n" + 
				"Fantastic Beasts and Where to Find Them\n" + 
				"Now You See Me";
				
		assertEquals(s, manager2.toString());
				
		for(int i = 0; i < 3; i++) //watches first three
			manager2.watchMovie();
		
		assertEquals("Moana",manager2.nextUnwatchedMovie().getTitle());
				
		for(int i = 3; i < movieList2.length; i++) //watches the rest of the movies
			manager2.watchMovie();
		
		assertEquals(null, manager2.nextUnwatchedMovie());
		
		manager2.addMovie("Back to the Future", "Robert Zemeckis", 1985);
		assertEquals(7,manager2.size());
		
		assertEquals("Back to the Future",manager2.nextUnwatchedMovie().getTitle());		
		
		manager2.watchMovie();		
		assertEquals(null, manager2.nextUnwatchedMovie());

	}
	
	@Test
	public void testMovieManagerEdge() {
		
		Movie one = new Movie("The Da Vinci Code", "Ron Howard", 2009); 
		Movie two = new Movie("The Avengers", "The Russo Brothers", 2012); 
		
		Movie[] movieList2 = {one, two, one};	// Note:  has a duplicate
				
		WebflixManager manager2 = new WebflixManager(movieList2);	
		
		/* Note:  If this fails, it means that your constructor is not enforcing the
		 * rules about no-duplicate movies (one of the class invariants).   I won't take 
		 * off for this (b/c the spec said it was ok), but you should think about how you could
		 * properly prevent this invariant condition (e.g. reuse your addmovie logic in this code)
		 */
		assertEquals(2,manager2.size());		


	}	

}
