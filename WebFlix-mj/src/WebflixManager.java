//Maggie Jiang
//1.17.19
//Period 1 APCS
//This class is a blueprint for WebflixManager objects

//parameters are not null
public class WebflixManager {
	
	//fields
	private Movie[] movieArray = new Movie[10];
	private int size;
	
	//constructs a WebflixManager
	public WebflixManager() {
		size=0;
	}
	
	//constructs a WebflixManager and fills the array with given Movies
	public WebflixManager(Movie[] movieList) {
		for (int i = 0; i<movieList.length; i++){
			movieArray[i]=movieList[i];
			size++;
			if(size==10)break; //adds first 10 if given list is greater than 10
		}	
	}
	
	//returns the number of movies in the Array (not the Array length)
	public int size(){
		return size;
	}
	
	//returns the first unwatched Movie in the list. if size is 0 or no unwatched movies, returns null.
	public Movie nextUnwatchedMovie(){
		for(int i = 0; i<size; i++){
			if (movieArray[i].isWatched()==false) return movieArray[i];
		}
		return null;
	}
	
	//if there is an unwatched movie, marks first unwatched movie as watched
	public void watchMovie(){
		if (this.nextUnwatchedMovie()!=null){
			this.nextUnwatchedMovie().watch();
		}
	}
	
	//adds a new movie to the end of the list if there is space and if the movie is not a duplicate
	public boolean addMovie(String title, String director, int year){
		Movie toAdd = new Movie(title, director, year);
		if(size==10) return false; //cant add if list is full
		for(int i = 0; i<size; i++){
			if (movieArray[i].isSame(toAdd)) return false; //cant add if there's a duplicate
		}
		movieArray[size]=toAdd;
		size++;
		return true;
	}
	
	//returns a string representation of the movie list
	public String toString(){
		if (size==0) return "No movies to show!";
		String toReturn = "";
		for(int i = 0; i<size-1; i++){ //builds string
			toReturn+=movieArray[i].getTitle() + "\n";
		}
		toReturn+=movieArray[size-1].getTitle(); //fencepost
		return toReturn;
	}
	
}
