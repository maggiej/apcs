//Maggie Jiang
//1.17.19
//Period 1 APCS
//This program tests the instance methods of the Movie and WebflixManager classes on Movie and WebflixManager objects

public class WebflixClient {
	
	public static void main(String[] args){
		//tests Movie object
		Movie testMovie = new Movie("Crazy Rich Asians", "Jon M. Chu", 2018); //tests constructor
		
		System.out.println(testMovie.getTitle()); //tests getTitle
		System.out.println(testMovie.getDirector()); //tests getDirector
		System.out.println(testMovie.getYearReleased()); //tests getYearReleased
		
		System.out.println(testMovie.isWatched()); //test isWatched
		testMovie.watch();
		System.out.println(testMovie.isWatched()); //tests watch
		
		//tests isSame
		Movie testIsSame = new Movie("Crazy Rich Asians", "Jon M. Chu", 2018);
		Movie testIsSame2 = new Movie("Selma", "Ava DuVernay", 2014);
		System.out.println(testMovie.isSame(testIsSame));
		System.out.println(testMovie.isSame(testIsSame2));
		
		System.out.println(testMovie); //tests toString
		
		//tests WebflixManager
		WebflixManager testManager = new WebflixManager(); //tests empty list
		System.out.println(testManager.size()); //tests size method
		System.out.println(testManager.nextUnwatchedMovie()); //should be null
		testManager.watchMovie(); //no effect
		System.out.println(testManager.nextUnwatchedMovie()); //shows that watchMovie had no effect.
		System.out.println(testManager.addMovie("Hello", "Bob", 3000)); //should be successful
		System.out.println(testManager);
		
		//test non empty WebflixManager
		Movie one = new Movie("Title1", "Director1", 2020);
		Movie two = new Movie("Title2", "Director2", 2021);
		Movie three = new Movie("Title3", "Director3", 2022);
		Movie list[] = {one, two, three};
		WebflixManager testManager2 = new WebflixManager(list);
		System.out.println(testManager2.size()); //3
		System.out.println(testManager2.nextUnwatchedMovie()); //1st one
		testManager2.watchMovie();
		System.out.println(testManager2.nextUnwatchedMovie()); //second one
		System.out.println(testManager2.addMovie("Hello", "Bob", 3000)); //true
		System.out.println(testManager2.addMovie("Hello", "Bob", 3000)); //false
		System.out.println(testManager2.addMovie("Title5", "Director5", 3000)); 
		System.out.println(testManager2.addMovie("Title6", "Director6", 3000)); 
		System.out.println(testManager2.addMovie("Title7", "Director7", 3000)); 
		System.out.println(testManager2.addMovie("Title8", "Director8", 3000)); 
		System.out.println(testManager2.addMovie("Title9", "Bob", 3000)); 
		System.out.println(testManager2.addMovie("Title10", "Bob", 3000)); 
		System.out.println(testManager2.addMovie("Title11", "Bob", 3000)); //false, max capacity
		System.out.println(testManager2);
	}	
}
	
